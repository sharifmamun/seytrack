/******************************************************************************
 SeyTrack
******************************************************************************/

function unixtime(){
    return Math.round((new Date()).getTime() / 1000);
}

var maint_mode = false;

var USER_TOKEN = null;
var map = null;
var setArrows = null;
var current_lines = [];
var active_marker = null;
var current_line_id = 0;

var saved_report_id = -1;
var current_report_attrib = "";
var current_report_devices = "";
var current_report_time = "";

var has_initial_pan = false;

var history_shown = false;
var history_scroller = null;
var history_playing = false;
var history_play_speed = 60;

var markers = [];
var current_markers = [];
var current_waypoints = [];
var last_pos = [];

var vehicle_names = [];
var vehicle_odo = [];
var vehicle_type = [];

//Current time as unix timestamp
var current_time = unixtime();
var time_offset = 0;

var marker_visible = [];

var geofence_string = "";

var isMobile = false;

var directions = null;

var show_waypoints = false;

var is_selected = false;

var follow_id = -1;

// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});


function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }
    else var expires = "";
    document.cookie = name+"="+value+expires+"; path=/";
}


function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name,"",-1);
}

/******************************************************************************
 Init the Google Map
******************************************************************************/
function build_map(){
    var myOptions = {
        center: new google.maps.LatLng(-4.68, 55.4619),
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        panControl: false
    };
    map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);

    //enter_edit_mode();

    directions = new google.maps.DirectionsService();

    setSizes();
}

var map_vis = true;

function toggle_map_vis(){
    if(map_vis){
        $("#map_canvas").css('z-index', -1);
        map_vis = false;
    }else{
        $("#map_canvas").css('z-index', 0);
        map_vis = true;
    }
}

function setSizes() {

    isMobile = $(window).width() < 940;

    var containerHeight = $(document).height();
    var offset = 40;

    if(isMobile){
        offset = 50; //responsive
    }
    $("#map_canvas").height(containerHeight - offset);

    $("#map_canvas").css('top', offset);


    if(isMobile){
        $("#movement_panel").css('width', '85%');
        $("#movement_panel").css('height', '90%');
        $("#movement_panel").css('left', '1%');
        $("#movement_panel").css('top', '1%');
    }
  
}

$(window).resize(function() { setSizes(); });




var edit_lat = 0;
var edit_lon = 0;


function enter_edit_mode(){

    $.getJSON("../api/api.py?op=get_snaps", function(data) {
        
        for(var i=0;i<data["result"].length;i++){

            var p = data["result"][i];

            var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(p["lat"], p["lon"]),
                    map: map
                });
            }
    });

    google.maps.event.addListener(map, 'mousemove', function(e) {
          //placeMarker(e.latLng, map);
          edit_lat = e.latLng.lat();
          edit_lon = e.latLng.lng();
    });

    $(document).keypress(function(event) {
        if(event.which == 108){

         
        $.getJSON("../api/api.py?op=add_snap&lat="+edit_lat+"&lon="+edit_lon, function(data) {
            
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(edit_lat, edit_lon),
                map: map
            });
        });

    }
    });

}

/*

         
        $.getJSON("../api/api.py?op=add_snap&lat="+lat+"&lon="+lon, function(data) {
            
            var marker = new google.maps.Marker({
                position: e.latLng,
                map: map
            });
        });
*/


/******************************************************************************
 Entry point
******************************************************************************/
function initialize() {

    $("#loading_background").hide();

	build_map();
    check_load();

    var cookie_username = readCookie("username");
    var cookie_password = readCookie("password");

    if(cookie_username != null){
        $("#login_email").val(cookie_username);
    }

    if(cookie_password != null){
        $("#login_password").val(cookie_password);
    }

    //enter_edit_mode();
    $("#navbar").show();

    $("#login_button").show();
    $("#login_spinner").hide();
    $("#login_spinner_mobile").hide();
    $("#loading_panel").fadeOut();

    $('#modalDevices').modal({show:false, backdrop:false, fade:false});
    $('#modalAbout').modal({show:false, backdrop:false, fade:false});
    $('#modalHelp').modal({show:false, backdrop:false, fade:false});

    $('#modalLogin').modal({show:false, backdrop:"static", fade:false});
    $('#modalMobileLogin').modal({show:false, backdrop:"static", fade:false});
    $('#modalReports').modal({show:false, backdrop:false, fade:false});
    $('#modalReportWizard3').modal({show:false, backdrop:false, fade:false});
    $('#modalReportWizard2').modal({show:false, backdrop:false, fade:false});
    $('#modalReportWizard1').modal({show:false, backdrop:false, fade:false});

    $("#initial_loading_text").hide();

/*
    $('#vehicle_filter').keyup(function() {
        filter_vehicles($('#vehicle_filter').val());
    });
*/

    google.maps.event.addListener(map, 'click', function(e) {
          clear_active_markers();
          follow_id = -1;
    });

    google.maps.event.addListener(map, 'drag', function(e) {
          follow_id = -1;
    });

    //TODO: load auth from cookie
    USER_TOKEN = null; //"DVeEigZKuCpOpEMsmzdpvHVHyKXtVfYofCWEUKcCwuToEudUSODRwXuOHdnZEfwN";

    //Log in if necessary
    if(USER_TOKEN == null){
       
        if(isMobile){
            $('#modalMobileLogin').modal('show');
        }else{
            $('#modalLogin').modal('show');
        }
    }else{
        login_success();   
    }

    /*

    $('#history_panel').mouseover(function() {
        $(this).stop(true).animate({opacity: 1.0}, 150);
      });
    $('#history_panel').mouseout(function() {
        $(this).stop(true).animate({opacity: 0.5}, 150);
      });

    */


    

}

function check_load(){
    $.getJSON("../api/api.py?op=get_load", function(data) {
        success = data["status"];

        if(success == false){
            return;
        }

        load = data["result"];

        if(load > 100){
            $("#login_load").show();
            $("#login_load_percent").html(load + "%");            
        }


    });
}


/******************************************************************************
 Get an auth token
******************************************************************************/
function begin_login(){

    if(maint_mode){
        alert("Currently we are peforming maintenance or applying updates -- please try again soon.\nSorry for the inconvenience.");
        return;
    }

    USER_TOKEN = null;

    $("#login_button").hide();
    $("#login_spinner").show();

    var username = $("#login_email").val();
    var password = $("#login_password").val();

    $.getJSON("../api/api.py?op=user_auth&email="+username+"&password="+password, function(data) {
        success = data["status"];

        if(success == false){
            alert("Bad username/password");

            $("#login_button").show();
            $("#login_spinner").hide();
            return;
        }

        USER_TOKEN = data["result"];

        createCookie("username", username, 30);
        createCookie("password", password, 30);

        login_success();

        //Save the details in a cookie
    });

}

function begin_login_mobile(){

    if(maint_mode){
        alert("Currently we are peforming maintenance or applying updates -- please try again soon.\nSorry for the inconvenience.");
        return;
    }

    USER_TOKEN = null;

    $("#login_button_mobile").hide();

    $("#report_chart_1").css("height", "200px");
    $("#report_chart_2").css("height", "200px");
    $("#report_chart_3").css("height", "400px");
    $("#report_details_1").html("");
    $("#report_details_2").html("");
    $("#report_details_3").html("");

    var username = $("#login_email_mobile").val();
    var password = $("#login_password_mobile").val();

    $.getJSON("../api/api.py?op=user_auth&email="+username+"&password="+password, function(data) {
        success = data["status"];

        if(success == false){
            alert("Bad username/password");

            $("#login_button_mobile").show();
            $("#login_spinner_mobile").hide();
            return;
        }

        USER_TOKEN = data["result"];

        createCookie("username", username, 30);
        createCookie("password", password, 30);

        login_success();
    });

}

function login_success(){
    load_devices(false);
      
    $('#modalLogin').modal('hide');

    $('#modalMobileLogin').modal('hide');

    //$("#map_canvas").show();

    $("#feedback_panel").show();

    //load_report_list();

    window.setInterval(periodic_update, 30000);

    if (!$.browser.msie) {
        window.setInterval(move_markers, 50);
    }

    load_notification_history();

    buildDateTimePicker();




    //showDateTimePicker("");

    //create_merged_report(10, unixtime()-(60*60*24*10), unixtime());

}

function follow(id){
    follow_id = id;

    for(var i=0;i<markers.length;i++){
        if(markers[i].id == id){
            map.panTo(markers[i].pos);
            markers[i].infowindow.close();
        }
    }

}

/******************************************************************************
 Cron
******************************************************************************/
function periodic_update(){

    if(history_shown == false){

        current_time = unixtime();

        load_devices(true);

        //if(current_line != null){
        //    load_details(current_line_id);
        //}
    }

    load_notification_history();


}

function move_markers(){

    for(var i=0;i<markers.length;i++){
        var marker = markers[i];

        if(marker.vel_steps > 0){
            marker.vel_steps--;

            marker.pos_x += marker.vel_x;
            marker.pos_y += marker.vel_y;

            var latlng = new google.maps.LatLng(marker.pos_x, marker.pos_y);
            marker.setPosition(latlng);

            marker.pos = latlng;

            if(marker.id == follow_id){
                map.panTo(latlng);
            }
        }
    }

}


/******************************************************************************
 Search box
******************************************************************************/
function update_filter(){

    for(var i=0;i<markers.length;i++){
        var marker = markers[i];
        if(marker_visible[marker.id] == true){
            markers[i].setVisible(true);
            jQuery('#filter_' + marker.id).attr('checked','checked');

        }else{
            markers[i].setVisible(false);
            jQuery('#filter_' + marker.id).removeAttr('checked');
        }
    }

}

function toggle_filter(mid){
    filter_marker(mid, jQuery('#filter_' + mid).is(':checked'));
}

function filter_marker(mid, visible){
    marker_visible[mid] = visible;

    update_filter();
}

function filter_show_all(){
    for(var i=0;i<markers.length;i++){
        var marker = markers[i];
        filter_marker(marker.id, true);
    }
}

function filter_show_none(){
    for(var i=0;i<markers.length;i++){
        var marker = markers[i];
        filter_marker(marker.id, false);
    }
}

function filter_hide_waypoints(){
    show_waypoints = true;
    filter_toggle_waypoints(); //hack!
}

function filter_show_waypoints(){
    show_waypoints = false;
    filter_toggle_waypoints(); //hack!
}

function filter_toggle_waypoints(){
    show_waypoints = !show_waypoints;

    if(show_waypoints){

        for(var i=0;i<current_waypoints.length;i++){
            current_waypoints[i].setMap(map);
        }
    }else{

        for(var i=0;i<current_waypoints.length;i++){
            current_waypoints[i].setMap(null);
        }
    }

}


/******************************************************************************
 Utility functions for cleanup
******************************************************************************/
function clear_active_markers(){

    is_selected = false;

   
    for(var i=0;i<current_lines.length;i++){
        current_lines[i].setMap(null);
    }
    current_lines = [];


    for(i=0;i<current_markers.length;i++){
        current_markers[i].setMap(null);
    }

    current_markers = [];



    for(i=0;i<current_waypoints.length;i++){
        current_waypoints[i].setMap(null);
    }

    current_waypoints = [];
}




/******************************************************************************
 History scroller
******************************************************************************/
function toggle_history_panel(){

    end_history_play();

    close_report();

    current_time = unixtime();

    if(history_shown){
        $("#history_panel").hide();
        history_shown = false;
        time_offset = 0;
        load_devices(false);
    }else{
        time_offset = 0;
        update_history_time(1);
        $("#history_panel").show();
        history_shown = true;

        if(history_scroller == null){

            
            history_scroller = new Dragdealer('my-slider',
            {
                "x":1,
                animationCallback: function(x, y)
                {
                    update_history_time(x);
                },
                callback: function(x, y){
                    clear_active_markers();
                    load_devices(false);
                }

            });



        }else{

            history_scroller.setValue(1);
        }
    }
}

function update_history_time(x){
    time_offset = ((1-x) * 60 * 60 * 24);
    timestring = timeConverter(current_time - time_offset);

    if(history_playing){
        timestring += " (" + Math.floor(history_play_speed / 60) + "x)";
    }

    $("#slider_text").html(timestring);
}


function finish_get_custom_history_time(time){
   
    if(isNaN(time)){
        alert("Couldn't understand that time!");
        return;
    }

    current_time = time;

    update_history_time(1);
    clear_active_markers();
    load_devices(false);


    history_scroller.setValue(1.0);

}


function get_custom_history_time(){

    showDateTimePicker(null, finish_get_custom_history_time);
}

function begin_history_play(){
    history_playing = true;

    $("#history_play").hide();
    $("#history_pause").show();

    setTimeout(function()
    {
       update_history_play();
    }, 1000);
}

function update_history_play(){

    current_time -= time_offset;
    current_time = current_time + history_play_speed;

    update_history_time(1);
    clear_active_markers();
    load_devices(false);

    time_offset = 0;


    if(history_playing){
        setTimeout(function()
        {
           update_history_play();
        }, 1000);

    }

}

function end_history_play(){
    history_playing = false;

    $("#history_play").show();
    $("#history_pause").hide();
}

function history_play_faster(){
    history_play_speed *= 2;
}

function history_play_slower(){
    history_play_speed /= 2;

    if(history_play_speed < 60){
        history_play_speed = 60;
    }
}


/******************************************************************************
 Load the device information and plot them on the map
******************************************************************************/
var has_controls_init = false;

function load_devices(allow_animation){
    
    begin_loading();

    $.getJSON("../api/api.py?op=get_devices&token="+USER_TOKEN+
        "&timestamp="+(current_time - time_offset), function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Couldn't get devices!");
            return;
        }

        var bounds = new google.maps.LatLngBounds();



        for(var i=0;i<markers.length;i++){
            markers[i].setMap(null);
        }
        markers = [];
        active_marker = null;

        var noti_html = "<select id=\"notification_vehicle\" onchange=\"notification_attrib_change()\">";
        noti_html += "<option value=\"0\">(any vehicle)</option>";

        var filter_html = "";

        for(i=0;i<data["result"].length;i++){
            var device = data["result"][i];

            noti_html += "<option value=\"" + device["id"] + "\">" + device["name"] + "</option>";

            filter_html += '<input onclick="toggle_filter(' + device["id"] + ')" id="filter_' + device["id"] + '" style="float: left; margin-right: 10px;" type="checkbox" name="v' + i + '" value="1" />';
            filter_html += '<label class="checkbox" style="color:#333" for="v' + i + '">' + device["name"] + '</label>';

            vehicle_names[device["id"]] = device["name"];
            vehicle_odo[[device["id"]]] = device["odo"];
            vehicle_type[[device["id"]]] = device["gpstype"];

            if(last_pos[device["id"]] === undefined || $.browser.msie || !allow_animation) {
                last_pos[device["id"]] = [device["lat"], device["lon"]];
            }

            var theLatLng = new google.maps.LatLng(last_pos[device["id"]][0], last_pos[device["id"]][1]);

            var marker = new google.maps.Marker({
                position: theLatLng,
                map: map,
                title:device["name"],
                icon:"markers/black/vehicle" + device["vehicletype"] + ".png"
            });

            bounds.extend(theLatLng);

            marker.pos = theLatLng;


            if(device["id"] == follow_id){
                map.panTo(theLatLng);
            }

            if(!$.browser.msie && allow_animation){

                //Movement
                marker.dest_pos_x = device["lat"];
                marker.dest_pos_y = device["lon"];
                marker.pos_x = last_pos[device["id"]][0];
                marker.pos_y = last_pos[device["id"]][1];
                marker.vel_steps = 600;

                marker.vel_x = (marker.dest_pos_x - marker.pos_x)/marker.vel_steps;
                marker.vel_y = (marker.dest_pos_y - marker.pos_y)/marker.vel_steps;

                last_pos[device["id"]] = [device["lat"], device["lon"]];
            }

            var fuelstr = summary_format_fuel(device["fuel"]);

            if(fuelstr == ""){
                fuelstr ='<a href="javascript:show_device_report(' + device["id"] + ',0);">View in report</a>';
            }

            var contentString = '<div id="content">'+
                '<div id="siteNotice">'+
                '</div>'+
                '<b id="firstHeading" class="firstHeading">'+device["name"] + '</b> - '+device["descr"]+
                '<div id="bodyContent">'+
                'Speed: <b>' + Math.round(device["speed"]) + ' km/h</b><br> '+
               // 'Fuel: <b>' + fuelstr + '</b><br>'+
                //'<p>Updated: <b>' + timeConverter(device["timestamp"]) + '</b><br> '+
                'Show report: '+
                '<a href="javascript:show_device_report(' + device["id"] + ',0);">Day</a>, '+
                '<a href="javascript:show_device_report(' + device["id"] + ',1);">Week</a>, '+
                '<a href="javascript:show_device_report(' + device["id"] + ',2);">Month</a><br>'+
                'Options: <a href=\"javascript:follow(' + device["id"] + ')">Follow</a>, <a href=\"javascript:show_movement_panel(' + device["id"] + ')\">Show movement</a>'+
                '</div>'+
                '</div>';
                
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.id = device["id"];
            marker.html = contentString;
            marker.filterstr = device["name"] + " " + device["descr"];
            marker.infowindow = infowindow;

            if(!has_controls_init){
                marker_visible[device["id"]] = true;
            }

            marker.activate = function(){
                infowindow.setContent(this.html);
                infowindow.open(map, this);
                load_details(this.id);
                active_marker = this;

                filter_hide_waypoints();
            };

            google.maps.event.addListener(marker, 'click', function() {
                this.activate();
            });


            google.maps.event.addListener(infowindow, 'closeclick', function() {
                //if(current_line != null){
                //    current_line.setMap(null);
                //}
                active_marker = null;
            });

            markers.push(marker);
        }

        noti_html += "</select>";

        if(!has_controls_init){
            $("#notification_device_list").html(noti_html);
            $("#filter_boxes").html(filter_html);

            $(function() {
              // Setup drop down menu
              $('.dropdown-toggle').dropdown();
             
              // Fix input element click problem
              $('.dropdown input, .dropdown label').click(function(e) {
                e.stopPropagation();
              });
            });

            has_controls_init = true;
        }

        update_filter();

        end_loading();

        if(!has_initial_pan){
            map.panTo(bounds.getCenter());
            has_initial_pan = true;
        }
    });
}


/******************************************************************************
 Periodic email dialog
******************************************************************************/
function load_email_dialog(){

    $('#modalEmailReports').modal('show');


    $("#modal_email_content").html('<img src="images/loading1.gif" height="32">');

    begin_loading();
    $.getJSON("../api/api.py?op=get_devices&token="+USER_TOKEN+
        "&timestamp="+(current_time - time_offset) , function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Couldn't get devices!");
            return;
        }

        var html = "";

        html += "<table class=\"table table-striped\">";
        html += "<thead>";
        html += "<tr>";
        html += "<th>Name</th>";
        html += "<th>Description</th>";
        html += "<th></th>"
        html += "</tr>";
        html += "</thead>";
        
        html += "<tbody>";

        for(var i=0;i<data["result"].length;i++){
            var device = data["result"][i];

            html += "<tr>";
            html += "<td>" + device.name + "</td>";
            html += "<td>" + device.descr + "</td>";
            html += "<td><label><input type='checkbox' id='email_report_enabled_" + device.id + "' ";

            if(device.report_every == 1){
                html += " checked=checked";
            }

            html += "/> Daily report enabled</label>";
            html += "</tr>";
        }


        html += "</tbody>";
        html += "</table>";

        $("#modal_email_content").html(html);

        end_loading();
    });
}

function save_email_reports(){

    var d_on = ",";
    var d_off = ",";

    for(var key in vehicle_names){
        var val = $("#email_report_enabled_" + key).is(':checked');
        
        if(val == true){
            d_on += key + ",";
        }else{
            d_off += key + ",";
        }
    }

    $.getJSON("../api/api.py?op=set_periodic_report&token="+USER_TOKEN+
        "&devices_on="+d_on+"&devices_off="+d_off , function(data) {
        success = data["status"];



        if(success == false){
            end_loading();
            alert("Couldn't set report settings!");
            return;
        }

        $('#modalEmailReports').modal('hide');

    });
}

/******************************************************************************
 Load the device information and add to the my vehicles dialog
******************************************************************************/
function summary_format_fuel(val){
    if(val <= 0){
        return "";
    }

    if(val > 100){
        return "100%";
    }

    return Math.round(val) + "%";
}

function summary_format_speed(val){
    if(val <= 0){
        return "stopped";
    }

    return Math.round(val) + "km/h";
}

function load_devices_dialog(){

    $("#modal_devices_content").html('<img src="images/loading1.gif" height="32">');

    begin_loading();
    $.getJSON("../api/api.py?op=get_devices&token="+USER_TOKEN+
        "&timestamp="+(current_time - time_offset)+"&full=true" , function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Couldn't get devices!");
            return;
        }

        var html = "";

        html += "<table class=\"table table-striped\">";
        html += "<thead>";
        html += "<tr>";
        html += "<th>Name</th>";
        html += "<th>Description</th>";
        html += "<th>Position</th>";
        html += "<th>Speed</th>";
        html += "<th>Milage</th>";
        html += "<th>Fuel</th>";
        html += "<th>Updated</th>";
        html += "<th>Report</th>";
        html += "</tr>";
        html += "</thead>";
        
        html += "<tbody>";

        for(var i=0;i<data["result"].length;i++){
            var device = data["result"][i];

            html += "<tr>";
            html += "<td>" + device.name + "</td>";
            html += "<td>" + device.descr + "</td>";
            html += "<td>" + device.placename + "</td>";
            html += "<td>" + summary_format_speed(device.speed) + "</td>";
            html += "<td>" + Math.round(device.odo) + "km</td>";
            html += "<td>" + summary_format_fuel(device.fuel) + "</td>";
            html += "<td>" + timeConverter(device.timestamp) + "</td>";
            html += "<td>" + '<a href="javascript:show_device_report(' + device["id"] + ',0);">Day</a>, '+
                '<a href="javascript:show_device_report(' + device["id"] + ',1);">Week</a>, '+
                '<a href="javascript:show_device_report(' + device["id"] + ',2);">Month</a></td>';
            html += "</tr>";
        }


        html += "</tbody>";
        html += "</table>";

        $("#modal_devices_content").html(html);

        end_loading();
    });
}


/******************************************************************************
 Load information on an individual device and plot it on the map
******************************************************************************/
function load_details(id){

    is_selected = true;

    filter_hide_waypoints();

    $("#movement_panel").hide();

    begin_loading();

    $.getJSON("../api/api.py?op=get_device_details&token="+USER_TOKEN+"&id="+id+
        "&timestamp="+(current_time - time_offset), function(data) {
        success = data["status"];


        if(success == false){
            end_loading();

            alert("Couldn't get details!");
            return;
        }

        current_line_id = id;


        var lineCoordinates = [];
        var arrowCoordinates = [];
        var detailLineCoordinates = [];

        var current_pos = null;

        for(var i=data["result"]["line"].length-1;i>=0;i--){
            var detail = data["result"]["line"][i];

            var pos = new google.maps.LatLng(detail["lat"], detail["lon"]);

            if(i == 0){
                current_pos = pos; //current vehicle position
            }
            
            /*
            if(i != 0){
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    title:timeConverter(detail["timestamp"])
                });
            }
            */

            lineCoordinates.push(pos);

        }

        for(var i=0;i<current_markers.length;i++){
            current_markers[i].setMap(null);
        }


        for(var i=0;i<current_waypoints.length;i++){
            current_waypoints[i].setMap(null);
        }

        current_markers = [];
        current_waypoints = [];


        for(var i=0;i<data["result"]["waypoints"].length;i++){
            var detail = data["result"]["waypoints"][i];

            var pos = new google.maps.LatLng(detail["lat"], detail["lon"]);
            
            detailLineCoordinates.push(pos);
       
            var marker = new google.maps.Marker({
                position: pos,
                icon:"http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_black.png",
                title:timeConverter(detail["timestamp"])
            });

            current_waypoints.push(marker);

            if(show_waypoints){
                marker.setMap(map);
            }

        }


        for(var i=data["result"]["stops"].length-1;i>=0;i--){
            var detail = data["result"]["stops"][i];

            var pos = new google.maps.LatLng(detail["lat"], detail["lon"]);
            
            
       
            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                icon:"http://maps.gstatic.com/mapfiles/ridefinder-images/mm_20_red.png",
                title:"Stopped here"
            });

            //alert(detail["lat"] + "," + detail["lon"] + ""
            
            current_markers.push(marker);

            var contentString = '<div id="content">'+
                '<div id="bodyContent">'+
                '<p><b>' + detail["text"] + "</b></p>" +
                '<p>Stopped here for ' + timeformat((detail["length"] * 30)) + '<br> '+
                'at ' + timeConverter(detail["when"]) + '</p>'+
                '</div>'+
                '</div>';

            if(detail["when"] == 0){
                contentString = '<div id="content">'+
                '<div id="bodyContent">'+
                 '<p><b>' + detail["text"] + "</b></p>" +
                '<p>Stopped here for more than ' + timeformat((detail["length"] * 30)) + ' '+
                //'starting more than 24h ago</p>'+
                '</div>'+
                '</div>';
            }
                
            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            marker.html = contentString;

            marker.activate = function(){
                infowindow.setContent(this.html);
                infowindow.open(map, this);
            };

            google.maps.event.addListener(marker, 'click', function() {
                this.activate();
            });

            
        }


        /*
        var line = new google.maps.Polyline({
          path: lineCoordinates,
          strokeWeight:2,
          map: map,
          strokeOpacity:0.6
        });

         if(current_line !== null){
            current_line.setMap(null);
        }
        current_line = line;
        */

        for(i=0;i<current_lines.length;i++){
            current_lines[i].setMap(null);
        }
        current_lines = [];

        if(vehicle_type[id] != 1){ //ie: not a boat
            add_routed_line(lineCoordinates, i, i+10, current_pos);
        }else{
            var line = new google.maps.Polyline({
              path: detailLineCoordinates,
              strokeWeight:2,
              map: map,
              strokeOpacity:0.6
            });

            current_lines.push(line);

        }


        end_loading();


    });


    
}

var route_cache = [];

function add_routed_line(coords, start, end, vehicle){

    if(!is_selected){
        end_loading();
        return;
    }

    var is_final = false;
    if(end >= coords.length-1){
        end = coords.length-1;
        is_final = true;
    }

    var waypoints = [];

    for(var i=start+1;i<end-1;i++){
        waypoints.push({
          location:coords[i],
          stopover:true
      });
    }

    var cache_id = coords[start].toString() + "," + coords[end].toString();

    if(route_cache[cache_id] != undefined && route_cache[cache_id] != null){
        var line = route_cache[cache_id];

        line.setMap(map);

        current_lines.push(line);

        if(!is_final){
            add_routed_line(coords, end, end+10, vehicle);
        }else{
            end_loading();
        }

        //alert("cache: " + cache_id);
        return;
    }

    var percentage = end / coords.length;

    directions.route({
        origin: coords[start],
        destination: coords[end],
        travelMode: google.maps.DirectionsTravelMode.DRIVING,
        waypoints: waypoints
    }, function(result, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            var path = [];
            path = path.concat(result.routes[0].overview_path);

            if(is_final){
                path.push(vehicle);
                //alert("final!");
            }

            var color = "#f00";

            if(percentage < 0.5){
                color = "#000";
                percentage += 0.5;
            }

            var opacity = percentage;

          
            var line = new google.maps.Polyline({
              path: path,
              strokeWeight:2,
              map: map,
              strokeOpacity:opacity,
              strokeColor:color
            });

            line.setMap(map);

            current_lines.push(line);

            route_cache[cache_id] = line;

            if(!is_final){
                //alert("carrying on!");
                setTimeout(function()
                {
                    add_routed_line(coords, end, end+10, vehicle);

                }, 100);

            }else{
                end_loading();
            }

        }else{
            if(status != google.maps.DirectionsStatus.ZERO_RESULTS){
                //alert("Failed to load snail trail from Google Maps API: " + err_to_string(status));  
                //end_loading();
                //return;

                setTimeout(function()
                {
                    add_routed_line(coords, end, end+10, vehicle);
                }, 1000);
                return;
            }   

            /*
            var arr = [];
            for(var i=end;i>=start;i--){
                arr.push(coords[i]);
            }
            //arr.push(coords[start]);
            //arr.push(coords[start+1]);

            

            var line = new google.maps.Polyline({
              path: arr,
              strokeWeight:2,
              map: map,
              strokeOpacity: opacity,
              strokeColor:"#666"
            });

            current_lines.push(line);
            */
            
            

            if(!is_final){
                //alert("carrying on!");
                //add_routed_line(coords, start+1, end-1, vehicle);

                setTimeout(function()
                {
                    add_routed_line(coords, start+2, end-2, vehicle);
                }, 100);
            }else{
                end_loading();
            }
    
        }
    });


}

function err_to_string(err){
    if(err == google.maps.DirectionsStatus.OK){
        return "ok";
    }

    if(err == google.maps.DirectionsStatus.NOT_FOUND){
        return "not found";
    }

    if(err == google.maps.DirectionsStatus.ZERO_RESULTS){
        return "zero results";
    }

    if(err == google.maps.DirectionsStatus.MAX_WAYPOINTS_EXCEEDED){
        return "max waypoints exceeded";
    }

    if(err == google.maps.DirectionsStatus.INVALID_REQUEST){
        return "invalid request";
    }

    if(err == google.maps.DirectionsStatus.OVER_QUERY_LIMIT){
        return "over query limit";
    }


    if(err == google.maps.DirectionsStatus.REQUEST_DENIED){
        return "denied";
    }


    if(err == google.maps.DirectionsStatus.UNKNOWN){
        return "server err";
    }

    return "unknown";

}

/******************************************************************************
 Create and show the devices dialog
******************************************************************************/
function show_devices(){
    close_report();
    load_devices_dialog();
    $('#modalDevices').modal('show');
}

/******************************************************************************
 Show the about window
******************************************************************************/
function show_about(){
    close_report();
    $('#modalAbout').modal('show');
}

function show_contact(){
    close_report();
    $('#modalContact').modal('show');
}

function show_help(){
    close_report();
    $('#modalHelp').modal('show');
}




/******************************************************************************
 Date/Time picker
******************************************************************************/

function buildDateTimePicker(){

    //Build the date/time picker
    var html = "";

    html += "<select style=\"width:50px\" id=\"datetime_time_hour\">";
    for(var i=1;i<=12;i++){
        html += "<option value=\"" + i + "\">" + i + "</option>";
    }
    html += "</select>";

    html += "<select style=\"width:50px\" id=\"datetime_time_minute\">";
    for(i=0;i<60;i++){
        if(i < 10){
            html += "<option value=\"0" + i + "\">0" + i + "</option>";
        }else{
             html += "<option value=\"" + i + "\">" + i + "</option>";
        }
    }
    html += "</select>";

    $("#time_control_area").html(html);



    html = "<select id=\"datetime_date\">";
    for(i=0;i<30;i++){
        var datestr = dateConverter(current_time - (i * 60 * 60 * 24));
        html += "<option value=\"" + datestr + "\">" + datestr + "</option>";
    }
    html += "</select>";

    $("#date_control_area").html(html);
}

var datetime_callback = null;

function showDateTimePicker(destid, callback){
    $('#modalDateTimePicker').modal('show');

    datetime_callback = callback;
}

function closeDateTimePicker(){
    $('#modalDateTimePicker').modal('hide');

    var hour = $("#datetime_time_hour").val();
    var min = $("#datetime_time_minute").val();
    var date = $("#datetime_date").val();
    var ampm = $("#datetime_time_ampm").val();

    string = date + ", " + hour + ":" + min + " " + ampm;
    parsed = Date.parse(string);

    unixtimestamp = Math.round(parsed / 1000);

    if(datetime_callback != null){
        datetime_callback(unixtimestamp);
    }

}

/******************************************************************************
 Reports wizard
******************************************************************************/
var device_checkbox_count = [];
var report_custom_start_time = null;
var report_custom_end_time = null;

function show_report_builder(){

    close_report();

    $('#modalReportWizard1').modal('show');

    begin_loading();

    $.getJSON("../api/api.py?op=get_devices&token="+USER_TOKEN+
        "&timestamp="+(current_time + time_offset), function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Couldn't get devices!");
            return;
        }

        var html = "";

        html += "<table class=\"table table-striped\">";
        html += "<thead>";
        html += "<tr>";
        html += "<th>Name</th>";
        html += "<th>Description</th>";
        html += "<th></th>";
        html += "</tr>";
        html += "</thead>";
        
        html += "<tbody>";

        device_checkbox_count = [];

        for(var i=0;i<data["result"].length;i++){
            var device = data["result"][i];

            device_checkbox_count.push(device.id);

            html += "<tr>";
            html += "<td>" + device.name + "</td>";
            html += "<td>" + device.descr + "</td>";
            html += "<td><input type=\"radio\" name=\"report_device\" value=\"" +
                     device.id + "\"></td>";
            html += "</tr>";
        }


        html += "</tbody>";
        html += "</table>";

        $("#modal_report_wizard_1_content").html(html);

        end_loading();
    });
}


function close_report(){

    $("#report_view").hide();
    $("#map_canvas").show();

    google.maps.event.trigger(map, 'resize');
    map.setZoom( map.getZoom() );

}

function print_report(){
    $("#report_modal_footer").hide();
    $("#report_details_1").hide();
    $("#report_details_2").hide();
    $("#report_details_3").hide();
    $("#navbar").hide();
    $("#print_panel").show();

    window.print();
}

function finish_printing(){
    $("#report_modal_footer").show();
    $("#report_details_1").show();
    $("#report_details_2").show();
    $("#report_details_3").show();
    $("#navbar").show();
    $("#print_panel").hide();
}


function report_time_changed(){

    var username = $("#report_time_range_preset").val();

    if(username == "custom"){
        $("#report_custom_time").show();
    }else{
        $("#report_custom_time").hide();
    }
}

function set_report_start_time(id){
    $('#modalReportWizard1').modal('hide');

    showDateTimePicker(null, finish_set_report_start_time);
}

function finish_set_report_start_time(time){

    if(isNaN(time)){
        alert("Couldn't understand that time!");
        return;
    }

    $("#report_custom_start_time").html("<a href=\"javascript:set_report_start_time()\">" + string + "</a>");

    report_custom_start_time = time;

    $('#modalReportWizard1').modal('show');

}


function set_report_end_time(id){
    $('#modalReportWizard1').modal('hide');

    showDateTimePicker(null, finish_set_report_end_time);
}

function finish_set_report_end_time(time){

    if(isNaN(time)){
        alert("Couldn't understand that time!");
        return;
    }

    $("#report_custom_end_time").html("<a href=\"javascript:set_report_end_time()\">" + string + "</a>");

    report_custom_end_time = time;

    $('#modalReportWizard1').modal('show');

}




/******************************************************************************
 Reports pin/unpin
******************************************************************************/
/*
function save_report(){


    $("#save_report_button").hide();
    $("#delete_report_button").show();

    $.getJSON("../api/api.py?op=save_report&attrib="+current_report_attrib+"&devices="+
                current_report_devices+"&time="+current_report_time+"&title=" + encodeURIComponent(report_title) +
                 "&token="+USER_TOKEN, function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Failed to save report");
            return;
        }

        saved_report_id = data["result"];

        load_report_list();
    });
}

function delete_report(){

    $("#save_report_button").show();
    $("#delete_report_button").hide();

    $.getJSON("../api/api.py?op=del_saved_report&id="+saved_report_id+"&token="+USER_TOKEN,
        function(data){
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Failed to delete report");
            return;
        }

        load_report_list();
    });
}

function load_report_list(){
    
    $.getJSON("../api/api.py?op=get_saved_reports&token="+USER_TOKEN+
        "&timestamp="+(current_time + time_offset), function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Couldn't get saved reports!");
            return;
        }

        var html = "";

        for(var i=0;i<data["result"].length;i++){
            var report = data["result"][i];

            html += "<li><a href=\"javascript:create_report(" +
                report["id"] + ");\">" + report["title"] + "</a></li>";
        }

        $("#saved_reports_list").html(html);

    });
}
*/




/******************************************************************************
 Reports renderer
******************************************************************************/
function show_device_report(device, time){

    $('#modalDevices').modal('hide');

   
    var timestart = null;
    var timeend = 0; //unixtime();
    
    if(time == 0){
        timestart = -(60 * 60 * 24);
    }
    else if(time == 1){
        timestart = -(60 * 60 * 24 * 7);
    }
    else if(time == 2){
        timestart = -(60 * 60 * 24 * 30);
    }

    create_merged_report(device, timestart, timeend);
}

function parse_time_val(id){

    value = $(id).val();
    parsed = Date.parse(value);

    var time = new Date(parsed).getTime();

    if(isNaN(time)){
        alert("Couldn't understand date: " + value);
        return 0;
    }

    return Math.round(time / 1000);
}

function create_report_from_form(){

    time = $("#report_time_range_preset").val();
    device = $('input:radio[name=report_device]:checked').val();

    if(device === undefined){
        alert("You need to select a device!");
        return;
    }

    var timestart = null;
    var timeend = 0; //unixtime();

    if(time == "custom"){
        timestart = report_custom_start_time;
        timeend = report_custom_end_time;
    }
    else if(time == "hour"){
        timestart = -(60 * 60);
    }
    else if(time == "today"){
        timestart = -(60 * 60 * 24);
    }
    else if(time == "week"){
        timestart = -(60 * 60 * 24 * 7);
    }
    else if(time == "month"){
        timestart = -(60 * 60 * 24 * 30);
    }

    $('#modalReportWizard1').modal('hide');

    create_merged_report(device, timestart, timeend);

}

function create_merged_report(vehicleid, timestart, timeend){

    //begin_loading();

    $("#report_data").html("Loading...");
    $("#report_title").html("Loading report...");

    $("#report_view").show();
    $("#map_canvas").hide();

    //normal
    if(vehicle_type[vehicleid] == 0){
        load_report_segment(vehicleid, timestart, timeend, "speed", 1);
        load_report_segment(vehicleid, timestart, timeend, "fuel", 2);
        load_report_segment(vehicleid, timestart, timeend, "milage", 3);
    }

    //boat
    else if (vehicle_type[vehicleid] == 1){
        load_report_segment(vehicleid, timestart, timeend, "speed", 1);
        load_report_segment(vehicleid, timestart, timeend, "uptime", 2);
        load_report_segment(vehicleid, timestart, timeend, "milage", 3);
    }

    //dozer
    else{
        load_report_segment(vehicleid, timestart, timeend, "uptime", 1);
        //load_report_segment(vehicleid, timestart, timeend, "fuel", 2);
        //load_report_segment(vehicleid, timestart, timeend, "milage", 3);

        hide_report_segment(2);
        hide_report_segment(3);
    }

/*
    load_report_segment(vehicleid, timestart, timeend, "fuel", 2);
    
    load_report_segment(vehicleid, timestart, timeend, "milage", 3);
*/
    load_movement_report(vehicleid, timestart, timeend, false);


}

function toggle_report_data(id){
    $("#report_data_" + id).toggle();
}

var movement_vid = 0;

function set_movement_report_time(id){
    $('#modalReportWizard1').modal('hide');

    showDateTimePicker(null, finish_set_movement_report_time);
}

function finish_set_movement_report_time(time){

    if(isNaN(time)){
        alert("Couldn't understand that time!");
        return;
    }

    load_movement_report(movement_vid, 0, time, false);
}



function load_movement_report(vehicleid, timestart, timeend, is_panel){

    $("#report_data_4").html("Loading movement data...");

    if(is_panel){
        $("#movement_panel_content").html("Loading movement...");
        $("#movement_panel").show();
    }

    movement_vid = vehicleid;

    url = "../api/api.py?op=get_movement_report&token="+USER_TOKEN+
            "&id="+vehicleid+"&timeend="+timeend;

    $.getJSON(url, function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Failed to get movement data");
            return;
        }

         if(!isMobile){

            //Header first



            var html = "";
            var cd = data["result"];

            if(!is_panel){

                html += "<h2>Movement report</h2>";
                html += "This shows the regions " + vehicle_names[vehicleid] + " has been in over the 24";
                html += " hour period ending " + timeConverter(cd[cd.length-1]["end_time"]);
                html += ". To view a different time, <a href='javascript:set_movement_report_time();'>click here</a><br><br>";

            }else{
                html += "This shows the regions " + vehicle_names[vehicleid] + " has been in over the 24";
                html += " hour period ending " + timeConverter(cd[cd.length-1]["end_time"]);            }

            html += "<table class=\"table table-bordered table-striped\">";
            html += "<thead>";
            html += "<tr><th></th><th>Region</th><th>Begin</th><th>End</th><th>Length</th>";

            if(!is_panel){
                html += "<th></th>";
            }

            if(cd.length > 0){
                //Table body
                html += "<tbody>";
                for(var i=cd.length-1;i>0;i--){
                    html += "<tr>";

                    if(cd[i]["type"] == "stop"){
                        html += "<td><i class='icon-stop'></i></td>";
                    }else{
                        html += "<td><i class='icon-play'></i></td>";
                    }

                    html += "<td>" + cd[i]["region"] + "</td>";
                    html += "<td>" + timeConverter(cd[i]["start_time"]) + "</td>";
                    html += "<td>" + timeConverter(cd[i]["end_time"]) + "</td>";
                    html += "<td>" + timeformat(cd[i]["length"]) + "</td>";

                    /*
                    if(!is_panel){
                        html += "<td style='width:500px'><a href='javascript:toggle_report_map(" + i + ")'>Toggle map</a>";

                        html += "<span style='display:none' id='report_map_" + i + "'>";
                        html += "<br><img src='http://maps.googleapis.com/maps/api/staticmap?center=" +
                                 cd[i]["lat"] + "," + cd[i]["lon"] +
                                 "&markers=color:red%7Clabel:S%7C" + cd[i]["lat"] + "," + cd[i]["lon"] +
                                 "&zoom=14&size=400x200&sensor=true'>";
                        html += "</span>";
                        html += "</td>";
                    }
                    */
                    html += "</tr>";
                }
                html += "</tbody></table>";

                //alert(html);
                
                //All done, set the html
                $("#report_data_4").html(html + "<br><br><br><br>");

                if(is_panel){
                    $("#movement_panel_content").html(html);
                }
            }else{  
                alert("no movement data");
                $("#report_data_4").html("");


                if(is_panel){
                    $("#movement_panel_content").html();
                }
            }
        }else{

            $("#report_data_4").html("");
            $("#report_data_4").hide();
        }
    });
}

function toggle_report_map(i){
    $("#report_map_" + i).toggle();
}

function show_movement_panel(vid){
    $("#movement_panel").show();
    load_movement_report(vid, 0, current_time, true);
}

function hide_movement_panel(){
    $("#movement_panel").hide();
}

function hide_report_segment(segid){

    $("#modal_data_" + segid).hide();
    $("#report_chart_" + segid).hide();
    $("#report_details_" + segid).hide();
}

function load_report_segment(vehicleid, timestart, timeend, attrib, segid){

    $("#modal_data_" + segid).show();
    $("#report_chart_" + segid).show();
    $("#report_details_" + segid).show();

    $("#modal_data_" + segid).html("Loading data...");
    $("#report_chart_" + segid).html("Loading data...");

    url = "../api/api.py?op=get_report_data&attrib="+attrib+
            "&devices="+vehicleid+"&time=custom|"+timestart+"|"+timeend;

    $.getJSON(url, function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Failed to get graph data");
            return;
        }


        var vMax = 100;

        if(attrib == "speed"){
            vMax = 120;
        }


        //Setup vars
        var options = {'title':data["result"]["title"],
                        'vAxis':{'minValue':0, 'maxValue':vMax},
                        'hAxis':{'minValue':0, 'maxValue':100}};

        if(isMobile){
            options["legend"] = {"position":"none"},
           // options["theme"] = "maximized";
           options["chartArea"] = {"width": '80%'}
        }
        
        //var report_title = data["result"]["title"];
        var attrib = data["result"]["attrib"];


        //Title        
        if(!isMobile){
            $("#report_title").html("<h1>" + data["result"]["report_title"] + "</h1>");
        }else{
            $("#report_title").html("<b>" + data["result"]["report_title"] + "</b><br>");
        }



        //Export button
        export_url = "../api/api.py?op=export_report_data&attrib="+attrib+
               "&devices="+vehicleid+"&time=custom|"+timestart+"|"+timeend;
        
        $("#report_export_"+segid).attr('href', export_url);

        if(data["result"]["data"].length <= 1 || data["result"]["data"][1].length == 0){
            $("#report_chart_" + segid).html("No data for that time period!");
        }else{
            var chart_data = google.visualization.arrayToDataTable(
                                            data["result"]["data"]);

            //Create the chart
            var chart = null;
            if(attrib == "milage"){
                chart = new google.visualization.BarChart(
                                document.getElementById('report_chart_' + segid));
            }else if(attrib == "fuel"){
                chart = new google.visualization.AreaChart(
                     document.getElementById('report_chart_' + segid));
            }
            else if(attrib == "uptime"){
                options['hAxis'] = {'minValue':0, 'maxValue':10};
                chart = new google.visualization.BarChart(
                     document.getElementById('report_chart_' + segid));
            }

            else{
                chart = new google.visualization.ColumnChart(
                    document.getElementById('report_chart_' + segid));
            }
            chart.draw(chart_data, options);

        }


        //Build the data table

        if(!isMobile){

            //Header first

            var html = "<table class=\"table table-bordered table-striped\">";
            html += "<thead>";
            html += "<tr>";
            var cd = data["result"]["data"];

            if(cd.length > 0){

                for(var i=0;i<cd[0].length;i++){
                    html += "<th>" + cd[0][i] + "</th>";
                }
                html += "</tr>";
                html += "</thead>";

                //Table body
                html += "<tbody>";
                for(var i=cd.length-1;i>0;i--){
                    html += "<tr>";
                    for(var x=0;x<cd[i].length;x++){
                        if( x > 0){
                            html += "<td>" + Math.round(cd[i][x]) + " " + data["result"]["unit"] + "</td>";
                        }else{
                            html += "<td>" + cd[i][x] + "</td>";
                        }
                    }
                    html += "</tr>";
                }
                html += "</tbody></table>";
                
                //All done, set the html
                $("#report_data_" + segid).html(html);
                $("#report_data_" + segid).hide();
            }else{  
                $("#report_data_" + segid).html("");
                $("#report_data_" + segid).hide();
            }
        }else{

            $("#report_data_" + segid).html("");
            $("#report_data_" + segid).hide();
        }

    });
}

/*
function create_report(reportid){

    begin_loading();


    $("#report_data").html("Loading...");
    $("#report_title").html("Loading report...");

    $("#report_view").show();
    $("#map_canvas").hide();
    $('#modalReportWizard3').modal('hide');

    var url = "";

    if(reportid < 0){

        attrib = $("#report_attribute").val();
        time = $("#report_time_range_preset").val();
        device = $('input:radio[name=report_device]:checked').val();

        url = "../api/api.py?op=get_report_data&attrib="+attrib+
                "&devices="+device+"&time="+time+"|"+
                report_custom_time["#report_start_time"]+"|"+
                report_custom_time["#report_end_time"];

        $("#save_report_button").show();
        $("#delete_report_button").hide();
    }else{
        url = "../api/api.py?op=get_saved_report&id=" + reportid;

        saved_report_id = reportid;

        $("#save_report_button").hide();
        $("#delete_report_button").show();
    }


    $.getJSON(url, function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Failed to get graph data");
            return;
        }  

        var chart_data = google.visualization.arrayToDataTable(data["result"]["data"]);

        var options = {
          
        };

        report_title = data["result"]["title"];

        attrib = data["result"]["attrib"];

        current_report_time = data["result"]["time"];
        current_report_attrib = data["result"]["attrib"];
        current_report_devices = data["result"]["devices"];

        export_html = "../api/api.py?op=export_report_data&attrib="+current_report_attrib+
                        "&devices="+current_report_devices+"&time="+current_report_time;
        $("#export_list").html("<li><a target=\"_blank\" href=\"" + export_html + "\">Export as CSV (excel)</a></li>");

        if(attrib == "milage"){

            var chart = new google.visualization.BarChart(document.getElementById('modal_report_chart'));
            chart.draw(chart_data, options);

        }else{

            var chart = new google.visualization.LineChart(document.getElementById('modal_report_chart'));
            chart.draw(chart_data, options);

        }

        var html = "<table class=\"table table-bordered table-striped\">";
        html += "<thead>";
        html += "<tr>";

        var cd = data["result"]["data"];

        for(var i=0;i<cd[0].length;i++){
            html += "<th>" + cd[0][i] + "</th>";
        }
        html += "</tr>";
        html += "</thead>";
        html += "<tbody>";

        for(var i=cd.length-1;i>0;i--){
            html += "<tr>";
            for(var x=0;x<cd[i].length;x++){
                if( x > 0){
                    html += "<td>" + Math.round(cd[i][x]) + " " + data["result"]["unit"] + "</td>";
                }else{
                    html += "<td>" + cd[i][x] + "</td>";
                }
                
            }
            html += "</tr>";
        }
        html += "</tbody></table>";
        
        $("#report_data").html(html);

        $("#report_title").html("<h1>" + data["result"]["title"] + "</h1>");

        end_loading();
      
    });
    
}
*/


/******************************************************************************
 Log out and show the login window
******************************************************************************/
function begin_logout(){

    //TODO: clear cookie

    USER_TOKEN = null;
   
    window.location.reload();
}



/******************************************************************************
 Notification windows
******************************************************************************/
function show_notification_triggers(){
    close_report();
    $('#modalNotificationTriggers').modal('show');

    load_notification_triggers();
}


function show_notifications(){
    close_report();
    load_notification_modal_history();
    $('#modalNotificationSummary').modal('show');
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}


/******************************************************************************
 Notifications
******************************************************************************/
function add_notification(){

    var vehicleid = $("#notification_vehicle").val();
    var attrib = $("#notification_attribute").val();
    var attribval = $("#notification_value").val();

    if(attrib == 3 || attrib == 4){
        attribval = geofence_string;
    }else{
        //must be a number
        if(!isNumber(attribval)){
            alert('"' + attribval + "\" is not a number. Please check the value and try again");
            return;
        }
    }

    var email = $('input[name=notification_cbemail]').is(':checked');
    var sms = $('input[name=notification_cbsms]').is(':checked');

    $.getJSON("../api/api.py?op=add_notification&token="+USER_TOKEN+
        "&vehicle="+vehicleid+"&attrib="+attrib+"&val="+attribval+"&email="+email+"&sms="+sms, function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Couldn't add notification!");
            return;
        }

        load_notification_triggers();

        $("#notification_value").val("");

    });
}

function notification_attrib_name(aid){
    if(aid =="1") return "Speed goes above";
    if(aid =="2") return "Fuel goes below";
    if(aid =="3") return "Goes out of area: ";
    if(aid =="4") return "Goes inside area: ";
    if(aid =="5") return "Milage goes above ";
    return aid;
}

function notification_value(str){
    if(str.indexOf("|") > 0){
        return str.substr(0, str.indexOf("|"));
    }
    return str;
}

function notification_notify_name(nid){
    if(nid == "e") return "Web, Email";
    if(nid == "s") return "Web, SMS";
    if(nid == "es" || nid == "se") return "Web, SMS, Email";
    return "Web";
}

function delete_notification_trigger(id){
    $.getJSON("../api/api.py?op=del_notification&token="+USER_TOKEN+
        "&id="+id, function(data) {
        success = data["status"];

        if(success == false){
            end_loading();
            alert("Couldn't del notification trigger!");
            return;
        }

        load_notification_triggers();

    });
}

function load_notification_triggers(){
   
    $.getJSON("../api/api.py?op=get_notification_triggers&token="+USER_TOKEN, function(data) {
        success = data["status"];

        html = "";

        if(success == false){
            end_loading();
            alert("Couldn't load notifications!");
            return;
        }

        for(var i=0;i<data["result"].length;i++){
            var trigger = data["result"][i];

            var name = vehicle_names[trigger["vehicleid"]];

            if(trigger["vehicleid"] == 0){
                name = "(Any vehicle)";
            }

            html += "<tr>";
            html += "<td>" + name + "</td>";
            html += "<td>" + notification_attrib_name(trigger["attrib"]) + " " + notification_value(trigger["value"]) + "</td>"
            html += "<td>" + notification_notify_name(trigger["notify"]) + "</td>";
            html += "<td><i class=\"icon-trash\"></i>";
            html += "<a href=\"javascript:delete_notification_trigger(" + trigger["id"] + ");\">Delete</a>";
            html += "</td>";
            html += "</tr>";
        }

        $("#notification_triggers_table").html(html);

    });   

}

function notification_attrib_change(){
    var attrib = $("#notification_attribute").val();
    var html = "";

    if(attrib == "1"){ //speed
        html = "km/h";
    }

    else if(attrib == "2"){ //fuel
        html = "%";
    }

    else if(attrib == "3" || attrib == "4"){ //geofence
        html = " <a href=\"javascript:generate_notification_geofence();\">"+
                    "Click here</a> to define the area";
    }

    else if(attrib == "5"){
        html = "km<br><br><b>Note: </b>Milage is cumulative since the start of SeyTrax installation. ";

        var device = $("#notification_vehicle").val();

        if(device > 0){
            html += "The current milage for " + vehicle_names[device] + " is " + Math.round(vehicle_odo[device]) + "km since installation";
        }

    }

    $("#notification_value_text").html(html);
    $("#notification_value").val("");

}

var geofence_listener = null;
var geofence_points = [];
var geofence_line = null;
var geofence_marker = null;

function clear_geofence(){
    geofence_points = [];

    if(geofence_marker != null){
        geofence_marker.setMap(null);
        geofence_marker = null;
    }

    if(geofence_line != null){
        geofence_line.setMap(null);
        geofence_line = null;
    }
}

function undo_geofence(){
    geofence_points.pop();
    update_geofence_list(null);

    if(geofence_points.length == 0 && geofence_marker != null){
        geofence_marker.setMap(null);
        geofence_marker = null;
    }
}

function finalize_geofence(){

    if(geofence_line != null){
        geofence_line.setMap(null);
    }

     // Construct the polygon
    geofence_line = new google.maps.Polygon({
      paths: geofence_points,
      strokeColor: '#FF0000',
      strokeOpacity: 0.8,
      strokeWeight: 2,
      fillColor: '#FF0000',
      fillOpacity: 0.35
    });

    geofence_line.setMap(map);
}

function update_geofence_list(ll){

    if(ll != null){
        geofence_points.push(ll);

        if(geofence_points.length == 1){
            //first point -- add a marker
            geofence_marker = new google.maps.Marker({
                position: ll,
                map: map,
                title: 'Click here to finalize the path'
            });

            google.maps.event.addListener(geofence_marker, 'click', function() {
                finalize_geofence();
            });
        }
    }

    if(geofence_line != null){
        geofence_line.setMap(null);
    }


    geofence_line = new google.maps.Polyline({
          path: geofence_points,
          strokeColor: '#FF0000',
          strokeOpacity: 1.0,
          strokeWeight: 2,
          map:map
        });

   

}

function generate_notification_geofence(){

    $('#modalNotificationTriggers').modal('hide');
    $("#geofence_panel").show();

    geofence_listener = google.maps.event.addListener(map, 'click', function(e) {
        update_geofence_list(e.latLng);
   });

    clear_geofence();

}

function cancel_generate_geofence(){

    google.maps.event.removeListener(geofence_listener);

    $("#geofence_panel").hide();

    clear_geofence();

}

function finish_generate_geofence(){

    if(geofence_points.length < 2){
        alert("Not enough points to add a geofence");
        return;
    }

    var name = prompt("Please name this area");

    if(name == "" || name == null){
        alert("You must provide a name!");
        return;
    }

    google.maps.event.removeListener(geofence_listener);

    var val = name + "|";
    for(var i=0;i<geofence_points.length;i++){
        val += geofence_points[i].lat() + "," + geofence_points[i].lng() + "|";
    }

    $("#notification_value").val(name); //val

    geofence_string = val;

    $('#modalNotificationTriggers').modal('show');
    $("#geofence_panel").hide();

    clear_geofence();


}

function load_notification_history(){
    $.getJSON("../api/api.py?op=get_notification_history&token="+USER_TOKEN, function(data) {
        success = data["status"];

        html = "";

        if(success == false){
            end_loading();
            alert("Couldn't add add_notification!");
            return;
        }

        new_html = "";
        old_html = "";

        new_count = 0;

        for(var i=0;i<data["result"].length;i++){
            var trigger = data["result"][i];

            html = "<li><a href=\"javascript:mark_notifications_read();\">" + trigger["text"] + " on " + timeConverter(trigger["when"]) + "</a></li>";

            if(trigger["unread"] == true){
                new_html += html;
                new_count ++;
            }else{
                old_html += html;
            }
        }

        new_html += "<li><a href=\"javascript:mark_notifications_read();\"><b>(Mark all as read)</b></a></li>";


        $("#new_notifications").html(new_html);
        //$("#old_notifications").html(old_html);

        if(new_count > 0){
            $("#unread_notifications_counter").html("(" + new_count + ")");

            window.document.title = "(" + new_count + ") SeyTrax | GPS vehicle tracking for the Seychelles";
        }else{
            $("#unread_notifications_counter").html("");

            window.document.title = "SeyTrax | GPS vehicle tracking for the Seychelles";

        }

    });
}


function load_notification_modal_history(){
    $.getJSON("../api/api.py?op=get_notification_history&limit=1000&token="+USER_TOKEN, function(data) {
        success = data["status"];

        var html = "<table class=\"table\">";
        html += "<thead> <tr><th><b>What</b></th><th><b>Details</b></th></tr></thead>";

        if(success == false){
            end_loading();
            alert("Couldn't add add_notification!");
            return;
        }

        html += "<tbody>";

        for(var i=0;i<data["result"].length;i++){
            var trigger = data["result"][i];

            html += "<tr><td>" + trigger["text"] + "</td><td>" + timeConverter(trigger["when"]) + "</td></tr>";
        }

        html += "</tbody></table>";

        $("#notification_modal_history").html(html);

    });




}

function mark_notifications_read(){
    $.getJSON("../api/api.py?op=mark_notifications_read&token="+USER_TOKEN, function(data) {
        success = data["status"];
        load_notification_history();
    });
}

/******************************************************************************
 Spinner
******************************************************************************/
var load = 0;

function begin_loading(){
    $("#loading_panel").fadeIn();

    load++;
}

function end_loading(){
    //load--;

    load = 0;

    if(load <= 0){
        $("#loading_panel").fadeOut();
        load = 0;
    }
}

/******************************************************************************
 Utils
******************************************************************************/
function timeConverter(UNIX_timestamp){
 var a = new Date(UNIX_timestamp*1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
     var year = a.getFullYear();
     var month = months[a.getMonth()];
     var date = a.getDate();
     var hour = a.getHours();
     var min = pad(a.getMinutes());
     var sec = pad(a.getSeconds());

     var ampm = "am";

     if(hour >= 12){
        ampm = "pm";
     }

     if(hour > 12){
        hour -= 12;
     }

     var time = date+' '+month+' '+year+', '+hour+':'+min+':'+sec+' '+ampm ;
     return time;
 }

 function timeConverter2(UNIX_timestamp){
 var a = new Date(UNIX_timestamp*1000);
     var year = a.getFullYear();
     var month = a.getMonth();
     var date = a.getDate();
     var hour = pad(a.getHours());
     var min = pad(a.getMinutes());

     var ampm = "AM";

     if(hour >= 12){
        ampm = "PM";
     }

     if(hour > 12){
        hour -= 12;
     }

     var time = month+'/'+date+'/'+year+', '+hour+':'+min+' '+ampm ;
     return time;
 }


 function dateConverter(UNIX_timestamp){
 var a = new Date(UNIX_timestamp*1000);
     var month = a.getMonth();
     var date = a.getDate();
    var year = a.getFullYear();


    var weekday = Array();

     weekday[0]="Sunday";
    weekday[1]="Monday";
    weekday[2]="Tuesday";
    weekday[3]="Wednesday";
    weekday[4]="Thursday";
    weekday[5]="Friday";
    weekday[6]="Saturday";

    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

     //var time = weekday[a.getDay()] + ", " + monthNames[month] +' '+ date;
    var time = date+' '+months[month]+' '+year;
     return time;
 }

function rand(from,to)
{
    return Math.floor(Math.random()*(to-from+1)+from);
}

function pad(n){
    if(n < 10){
        return "0" + n;
    }else{
        return n;
    }
}

function timeformat(seconds){
    if(seconds < 60){
        return seconds + " seconds";
    }

    if(seconds < 60 * 60){
        return Math.round(seconds / 60) + " minutes";
    }

    var num = Math.round(seconds / (60 * 60));

    if(num == 1){
        return "1 hour";
    }

    return num + " hours";
}




/******************************************************************************
 Arrow magic - http://www.wolfpil.de/v3/arrow-heads.html
******************************************************************************/
// Polyline with arrows
//
// Bill Chadwick May 2008 (Ported to v3 by Peter Bennett)
// 
// Free for any use
//


//Constructor
function BDCCArrowedPolyline(points, color, weight, opacity, opts, gapPx, headLength, headColor, headWeight, headOpacity) {     
    
    this.gapPx = gapPx;
    this.points = points;
    this.color = color;
    this.weight = weight;
    this.opacity = opacity;
    this.headLength = headLength;
    this.headColor = headColor;
    this.headWeight = headWeight;
    this.headOpacity = headOpacity;
    this.opts = opts;
    this.heads = new Array();
    this.line = null;
    this.map = map;   
         this.setMap(map);       
         this.prj = this.getProjection();        
}

// Overlay v3
BDCCArrowedPolyline.prototype = new google.maps.OverlayView();

// onRemove function
BDCCArrowedPolyline.prototype.onRemove = function() {
        
    try{
        if (this.line)
            //this.map.removeOverlay(this.line);
                                this.line.setMap(null);
        for(var i=0; i<this.heads.length; i++)
            //this.map.removeOverlay(this.heads[i]); 
                                this.heads[i].setMap(null);
    }
    catch(ex)
    {
    }

};

BDCCArrowedPolyline.prototype.onAdd = function() {

   //this.onRemove();
   
   this.line = new google.maps.Polyline({
                        clickable:false,
                                        editable:false,
                                        geodesic:false,
                                        map:this.map,
                                        path:this.points,
                                        strokeColor:this.color,
                                        strokeOpacity:this.opacity,
                                        strokeWeight:this.weight,
                                        visible:true
                                        });
        
};

BDCCArrowedPolyline.prototype.draw = function() {
        
   var zoom = this.map.getZoom();
   
   // the arrow heads
   this.heads = new Array();
   this.headid = 0;
   
   var p1 = this.getProjection().fromLatLngToContainerPixel(this.points[0]);//first point
   
   var p2;//next point
   var dx;
   var dy;
   var sl;//segment length
   var theta;//segment angle
   var ta;//distance along segment for placing arrows
   
   for (var i=1; i<this.points.length; i++){
            
          p2 = this.getProjection().fromLatLngToContainerPixel(this.points[i]);
      dx = p2.x-p1.x;
      dy = p2.y-p1.y;

        if (Math.abs(this.points[i-1].lng() - this.points[i].lng()) > 180.0)
                dx = -dx;

      sl = Math.sqrt((dx*dx)+(dy*dy)); 
      theta = Math.atan2(-dy,dx);
      


      j=1;
      
        if(this.gapPx == 0){
                //just put one arrow at the end of the line
                this.addHead(p2.x,p2.y,theta,zoom);
        }
        else if(this.gapPx == 1) {
                //just put one arrow in the middle of the line
                var x = p1.x + ((sl/2) * Math.cos(theta)); 
                var y = p1.y - ((sl/2) * Math.sin(theta));
                this.addHead(x,y,theta,zoom);        
        }
        else{
        //iterate along the line segment placing arrow markers
        //don't put an arrow within gapPx of the beginning or end of the segment 

              ta = this.gapPx;
        while(ta < sl){
                var x = p1.x + (ta * Math.cos(theta)); 
                var y = p1.y - (ta * Math.sin(theta));
                this.addHead(x,y,theta,zoom);
                ta += this.gapPx;  
        }  
      
                //line too short, put one arrow in its middle
                
        if(ta == this.gapPx){
                        var x = p1.x + ((sl/2) * Math.cos(theta)); 
                        var y = p1.y - ((sl/2) * Math.sin(theta));
                        this.addHead(x,y,theta,zoom);        
                    
             }
        }
      
      p1 = p2;   
   }
        
};

BDCCArrowedPolyline.prototype.addHead = function(x,y,theta,zoom) {

        
    //add an arrow head at the specified point
    var t = theta + (Math.PI/4) ;
    if(t > Math.PI)
        t -= 2*Math.PI;
    var t2 = theta - (Math.PI/4) ;
    if(t2 <= (-Math.PI))
        t2 += 2*Math.PI;
    var pts = new Array();
    var x1 = x-Math.cos(t)*this.headLength;
    var y1 = y+Math.sin(t)*this.headLength;
    var x2 = x-Math.cos(t2)*this.headLength;
    var y2 = y+Math.sin(t2)*this.headLength;
    pts.push(this.getProjection().fromContainerPixelToLatLng(new google.maps.Point(x1,y1)));
    pts.push(this.getProjection().fromContainerPixelToLatLng(new google.maps.Point(x,y)));    
    pts.push(this.getProjection().fromContainerPixelToLatLng(new google.maps.Point(x2,y2)));

        var arrow = new google.maps.Polyline({
                clickable:false,
                                         editable:false,
                                         geodesic:false,
                                         path:pts,
                                         strokeColor:this.color,
                                         strokeOpacity:this.opacity,
                                         strokeWeight:this.weight,
                                         visible:true
                                         });
 
    this.heads.push(arrow);
         
        this.heads[this.heads.length-1].setMap(this.map);
         
};

