#!/usr/bin/python

###############################################################################
# SeyTrack API
###############################################################################
import cgitb
import cgi
import json
import db
import bcrypt
import random
import time
import string
import importer
import datetime
import urllib
import math
import config
import subprocess
import base64
import simplejson
import os
import sys
import traceback


###############################################################################
# Error handling
###############################################################################
cgitb.enable()


class APIException(Exception):
    def __init__(self, value):
        self.value = value

    def __str__(self):
        return repr(self.value)


def printe(str):
    sys.stderr.write(str + '\n')

###############################################################################
# Entry points
###############################################################################
def main():

    #time.sleep(2)

    try:
        ret = handle_op()
        return {"status": True, "result": ret}
    except APIException, e:
        return {"status": False, "result": None, "error": str(e)}


###############################################################################
# Operations dispatcher
###############################################################################
def handle_op():
    form = cgi.FieldStorage()

    if "op" not in form:
        raise APIException("Requires op parameter")

    params = {}
    for k in form.keys():
        params[k] = form[k].value

    op = "op_" + form["op"].value

    if op not in globals():
        raise APIException("Bad operation: " + op)

    if config.USE_MIXPANEL:
        track(op, params)

    res = globals()[op](params)

    return res

###############################################################################
# Server laod
###############################################################################
def op_get_load(params):
    return os.getloadavg()[1] * 50  # percentage, two cores

###############################################################################
# Users
###############################################################################
def pwhash(password):
    hashed = bcrypt.hashpw(password, bcrypt.gensalt())
    return hashed


def pwcheck(password, hashed,):
    if bcrypt.hashpw(password, hashed) == hashed:
        return True
    else:
        return False


def get_userid_by_email(email):
    r = db.query("SELECT id FROM users WHERE email = %s", (email))
    return r[0][0]


def get_user_email_by_id(id):
    r = db.query("SELECT email FROM users WHERE id = %s", (id))
    return r[0][0]


def get_user_name_by_id(id):
    r = db.query("SELECT name FROM users WHERE id = %s", (id))
    return r[0][0]


def add_auth_token(userid):
    token = ''.join(random.choice(string.letters) for i in xrange(64))
    db.query("INSERT INTO authtokens(user, timestamp, token) " +\
        "VALUES(%s, %s,%s)", (userid, time.time(), token))
    return token


def check_auth_token(token):
    r = db.query("SELECT user FROM authtokens WHERE token = %s", (token))
    if len(r) == 0:
        return -1
    return r[0][0]


def require_valid_user(params):
    if "token" not in params:
        raise APIException("Requires authentication")

    uid = check_auth_token(params["token"])

    if uid == -1:
        raise APIException("Invalid authentication token")
    return uid


def op_user_add(params):

    phash = pwhash(params["password"])

    db.query("INSERT INTO users(name, email, password, usertype, useralias) " +\
        "VALUES(%s, %s,%s,%s,%s)",
            (params["name"], params["email"], phash, params["usertype"], params["useralias"]))

    update_seytrax_googledoc_new_user(params)

    return True

def op_user_del(params):
    db.query("DELETE from users WHERE id = %s", (params["id"]))
    return True



def op_user_setting(params):

    if "value" in params:
        val = params["value"]
        key = params["key"]

        if key == "password":
            val = pwhash(val)

        r = db.query("UPDATE users SET " + key + " = %s WHERE id = %s", (val, params["id"]))
        return True

    else:
        r = db.query("SELECT " + params["key"] + " FROM users WHERE id = %s", (params["id"]))
        return r[0][0]


def op_user_auth(params):

    r = db.query("SELECT password,usertype,useralias FROM users WHERE email = %s", (params["email"]))

    if len(r) == 0:
        #no such user in database
        raise APIException("Invalid username/password")

    for user in r:

        pwok = pwcheck(params["password"], user[0])

        if pwok == False:
            continue

        usertype = user[1]
        useralias = user[2]

        token = None

        if usertype == 0:
            token = add_auth_token(get_userid_by_email(params["email"]))
        elif usertype == 1:
            token = add_auth_token(useralias)

        return token

    raise APIException("Invalid username/password")


def op_generate_admin_token(params):

    token = add_auth_token(params["id"])

    return token


def op_user_list(params):

    r = db.query("SELECT id,name,email,useralias,usertype FROM users", None)

    ret = []

    for entry in r:
        user = {}
        user["id"] = entry[0]
        user["name"] = entry[1]
        user["email"] = entry[2]
        user["useralias"] = entry[3]
        user["usertype"] = entry[4]
        ret.append(user)
    return ret


###############################################################################
# Daily report stuff
###############################################################################
def op_set_periodic_report(params):

    for d in params["devices_on"].split(","):
        db.query("UPDATE devices SET report_every = 1 WHERE id = %s", (d))

    for d in params["devices_off"].split(","):
        db.query("UPDATE devices SET report_every = 0 WHERE id = %s", (d))

    return True


def op_get_periodic_report(params):
    uid = require_valid_user(params)

    r = []

    for d in db.query("SELECT id FROM devices WHERE owner = %s AND report_every = 1", (uid)):
        r.append(d[0])

    return r


def op_send_daily_reports(params):
    for d in db.query("SELECT id,owner,name FROM devices WHERE report_every = 1", ()):
        send_daily_report(d[0], d[1], d[2])

    return True


def timeformat(seconds):
    if seconds < 60:
        return str(seconds) + " seconds"

    if seconds < 60 * 60:
        return str(int(seconds / 60)) + " minutes"

    return str(int(seconds / (60 * 60))) + " hour(s)"


import smtplib
from email.MIMEMultipart import MIMEMultipart
from email.MIMEBase import MIMEBase
from email.MIMEText import MIMEText
from email.Utils import COMMASPACE, formatdate
from email import Encoders

def send_daily_report(dev, uid, devname, actually_send = True):


    params = {}
    params["id"] = dev
    params["timeend"] = time.time()
    pos_data = op_get_movement_report(params)

    if not actually_send:
        print devname
        return

    r = db.query("SELECT email FROM users WHERE id=%s", (uid))
    user_email = r[0][0]

    #if user_email == "test@seytrack.com":
    #user_email = "paul@bieh.net"

  
    sender = 'paul@bieh.net'
    subject = '[SeyTrax] Daily report for ' + devname
    body = "This is the movement report for " + devname + " for the last 24 hours.\r\n\r\n"

    text = ""
    csv = ""

    for p in pos_data[::-1]:
        text += timestr(p["start_time"]) + " to " + timestr(p["end_time"]) + " -- in " + p["region"] + " for " + timeformat(p["length"]) + "\r\n"
        csv += p["region"].replace(",", " ") + "," + timestr(p["start_time"]).replace(",", " ") + "," + timestr(p["end_time"]).replace(",", " ") + "," + timeformat(p["length"]).replace(",", " ") + "\n"

    body += text

    SMTP_SERVER = 'smtp.gmail.com'
    SMTP_PORT = 587

    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)

    session.ehlo()
    session.starttls()
    session.ehlo
    session.login(sender, "tr5aswou")

    msg = MIMEMultipart()
    msg['From'] = sender
    msg['To'] = user_email
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(body))

    part = MIMEBase('application', "octet-stream")
    part.set_payload(csv)
    Encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="movement.csv"')
    msg.attach(part)

    session.sendmail(sender, user_email, msg.as_string())
    session.close()


###############################################################################
# Device wizard stuff for the admin interface
###############################################################################
import gdata.docs
import gdata.docs.service
import gdata.spreadsheet.service
import gdata.spreadsheet.text_db
import re




def update_seytrax_googledoc(params):
    username = 'paul@bieh.net'
    passwd = 'tr5aswou'
    doc_name = 'TestSheet'

    # Connect to Google
    gd_client = gdata.spreadsheet.service.SpreadsheetsService()
    gd_client.email = username
    gd_client.password = passwd
    gd_client.source = 'seytrax'
    gd_client.ProgrammaticLogin()

    q = gdata.spreadsheet.service.DocumentQuery()
    q['title'] = doc_name
    q['title-exact'] = 'true'
    feed = gd_client.GetSpreadsheetsFeed(query=q)
    spreadsheet_id = feed.entry[0].id.text.rsplit('/', 1)[1]
    feed = gd_client.GetWorksheetsFeed(spreadsheet_id)
    worksheet_id = feed.entry[0].id.text.rsplit('/', 1)[1]

    data = {}
    data["username"] = ""
    data["contactphonenumber"] = ""
    data["licenseplate"] = params["descr"]
    data["dateinstalled"] = datetime.datetime.fromtimestamp(time.time()).strftime('%m/%d/%Y')
    data["company"] = get_user_name_by_id(params["uid"])
    data["contactperson"] = ""
    data["imei"] = params["imei"]
    data["deviceref"] = params["name"]
    data["make"] = params["name"]
    data["password"] = ""
    data["phonenumber"] = params["phone"]

    entry = gd_client.InsertRow(data, spreadsheet_id, worksheet_id)

    if isinstance(entry, gdata.spreadsheet.SpreadsheetsList):
        return True
    else:
        return False


def update_seytrax_googledoc_new_user(params):
    username = 'paul@bieh.net'
    passwd = 'tr5aswou'
    doc_name = 'TestSheet'

    # Connect to Google
    gd_client = gdata.spreadsheet.service.SpreadsheetsService()
    gd_client.email = username
    gd_client.password = passwd
    gd_client.source = 'seytrax'
    gd_client.ProgrammaticLogin()

    q = gdata.spreadsheet.service.DocumentQuery()
    q['title'] = doc_name
    q['title-exact'] = 'true'
    feed = gd_client.GetSpreadsheetsFeed(query=q)
    spreadsheet_id = feed.entry[0].id.text.rsplit('/', 1)[1]
    feed = gd_client.GetWorksheetsFeed(spreadsheet_id)
    worksheet_id = feed.entry[0].id.text.rsplit('/', 1)[1]

    data = {}
    data["username"] = params["email"]
    data["contactphonenumber"] = ""
    data["licenseplate"] = ""
    data["dateinstalled"] = ""
    data["company"] = params["name"]
    data["contactperson"] = ""
    data["imei"] = ""
    data["deviceref"] = ""
    data["make"] = ""
    data["password"] = params["password"]
    data["phonenumber"] = ""

    entry = gd_client.InsertRow(data, spreadsheet_id, worksheet_id)

    if isinstance(entry, gdata.spreadsheet.SpreadsheetsList):
        return True
    else:
        return False

def sms_log(txt):
    f = open("sms.log", "a")
    f.write(txt + "\n")
    f.close()

def send_wizard_sms(number, message, src):
    url = "http://rest.nexmo.com/sms/json?" +\
            "username=a1a1bb00&" +\
            "password=40521011&" +\
            "from=" + src + "&" +\
            "to=" + number + "&" +\
            "text=" + urllib.quote_plus(message)

    #sms_log(url)

    response = urllib.urlopen(url).read()

    data = json.loads(response)
    #data = "no sending at the moment"

    return data




def op_reset_device(params):

    txts = ["*000000,990,099#",
            "*000000,011,internet,,#",
            "*000000,015,0,54.243.41.201,12345#",
            "*000000,016,1#",
            "*000000,018,30,999#"]

    sms_log("")
    sms_log("")

    params["phone"] = params["phone"].strip()
    params["srcphone"] = params["srcphone"].strip()

    last_reset = db.query("SELECT last_reset FROM devices WHERE phone = %s", (params["phone"]))

    if len(last_reset) > 0 and len(last_reset[0]) > 0:
        last_reset = last_reset[0][0]
        diff = time.time() - last_reset

        if diff < (60 * 10):
            sms_log(params["phone"] + ": **** device reset too soon! " + str(params) + str(diff))
            return "Device reset too soon! was " + str(diff) + " ago, must be at least 600"
    
    db.query("UPDATE devices SET last_reset = %s WHERE phone = %s", (time.time(), params["phone"]))

    sms_log(params["phone"] + ": **** resetting device " + str(params))

    for txt in txts:
        send_wizard_sms(params["phone"].strip(), txt, params["srcphone"].strip())
        sms_log(params["phone"] + ": Sent " + txt)
        time.sleep(20)

    return True

def op_add_device_wizard(params):
    print "Content-Type: text/plain\r\n"

    print "***************************"
    print "Adding device, please wait"
    print "***************************\n"

    op_add_device(params)

    print "- Added to SeyTrax database"

    doc_success = update_seytrax_googledoc(params)

    if not doc_success:
        print "Couldn't update the google doc!"

    print "- Added to Google Doc"

    txts = ["*000000,011,internet,,#",
            "*000000,015,0,54.243.41.201,12345#",
            "*000000,016,1#",
            "*000000,018,30,999#"]

    for txt in txts:
        send_wizard_sms(params["phone"], txt, params["srcphone"])
        print "- Sent " + txt
        time.sleep(60)

    print "All done!"

    sys.exit(0)

    return True


###############################################################################
# Devices
###############################################################################
def op_add_device(params):
    uid = params["uid"]

    phone = ""
    if "phone" in params:
        phone = params["phone"]

    db.query("INSERT INTO devices(gpstype,vehicletype,name,descr,owner,imei,fuel_min,fuel_max,phone) " +\
        "VALUES(%s, %s,%s,%s,%s,%s,%s,%s,%s)",
            (params["gpstype"], params["vehicletype"],
                params["name"], params["descr"], str(uid), params["imei"],
                params["min_fuel"], params["max_fuel"], phone))

    did = db.get_last_id()

    db.query("\
                CREATE TABLE IF NOT EXISTS `updates_" + str(did) + "` (\
                  `id` int(11) NOT NULL AUTO_INCREMENT,\
                  `device` int(11) NOT NULL,\
                  `timestamp` int(11) NOT NULL,\
                  `lat` float NOT NULL,\
                  `lon` float NOT NULL,\
                  `speed` float NOT NULL,\
                  `fuel` float NOT NULL,\
                  PRIMARY KEY (`id`),\
                  KEY `timestamp` (`timestamp`),\
                  KEY `device` (`device`)\
                )", ())

    return True

def op_del_device(params):
    uid = params["id"]

    db.query("DELETE from devices WHERE id = %s", (uid))
    return True


def op_device_list(params):

    r = db.query("SELECT id,name,descr,phone FROM devices", None)

    ret = []

    for entry in r:
        user = {}
        user["id"] = entry[0]
        user["name"] = entry[1]
        user["descr"] = entry[2]
        user["phone"] = entry[3]
        ret.append(user)
    return ret


def op_device_list_ex(params):

    r = db.query("SELECT id,name,descr,owner,last_update_id FROM devices ORDER BY owner", None)

    ret = []

    if "start" in params:

        start = int(params["start"])
        end = int(params["end"])
    else:
        start = 0
        end = 9999

    for entry in r[start:end]:
        user = {}
        user["id"] = entry[0]
        user["name"] = entry[1]
        user["descr"] = entry[2]

        #details = db.query("SELECT timestamp FROM updates" +\
        #                    " WHERE id = %s", (entry[4]))

        details = db.query("SELECT timestamp FROM updates" +\
                            " WHERE device = %s " +\
                            "ORDER BY id DESC LIMIT 1", (entry[0]))

        owner_details = db.query("SELECT email FROM users WHERE id = %s", (entry[3]))

        if len(details) > 0:
            user["last_update"] = timestr(details[0][0])
            user["last_update_timestamp"] = details[0][0]
            user["last_update_diff"] = time.time() - details[0][0]
        else:
            user["last_update"] = "never"
            user["last_update_timestamp"] = -1
            user["last_update_diff"] = -1

        if len(owner_details) > 0:
            user["owner"] = owner_details[0][0]
        else:
            user["owner"] = "unknown"

        ret.append(user)
    return ret


def get_device_name(devid):
    return db.query("SELECT name FROM devices WHERE id=%s", (devid))[0][0]


def get_last_fuel(devid):
    f = db.query("SELECT fuel FROM updates WHERE device=%s ORDER BY id DESC LIMIT 100", (devid))

    for i in f:
        if i[0] > 0:
            return fuel_rescale_ex(devid, i[0])
    return 0


def op_get_devices(params):
    uid = require_valid_user(params)
    if "timestamp" not in params:
        params["timestamp"] = time.time()
    r = db.query("SELECT gpstype,vehicletype,name,descr,id,odo,report_every,last_update_id FROM devices WHERE owner = %s", (uid))

    devices = []

    for device in r:

        #time_diff = int(params["timestamp"]) - time.time()

        #if time_diff > (60 * 10):
        #    details = db.query("SELECT lat,lon,timestamp,speed FROM updates" +\
        #                    " WHERE device = %s" +\
        #                    " AND timestamp < %s " +\
        #                    "ORDER BY timestamp DESC LIMIT 1", (device[4], int(params["timestamp"])))
        #else:
        #   details = db.query("SELECT lat,lon,timestamp,speed FROM updates WHERE device = %s AND id = %s", (device[4], device[7]))

        details = db.query("SELECT lat,lon,timestamp,speed FROM updates" +\
                            " WHERE device = %s" +\
                            " AND timestamp < %s " +\
                            "ORDER BY timestamp DESC LIMIT 1", (device[4], float(params["timestamp"])))

        if len(details) <= 0:
            details = db.query("SELECT lat,lon,timestamp,speed FROM updates_" + str(device[4]) +\
                            " WHERE timestamp < %s " +\
                            "ORDER BY timestamp DESC LIMIT 1", (float(params["timestamp"])))
            if len(details) == 0:
                continue

        lat = details[0][0]
        lon = details[0][1]

        #fueldetails = db.query("SELECT fuel FROM updates" +\
        #                    " WHERE device = %s" +\
        #                    " AND timestamp < %s AND fuel > 0 " +\
        #                    "ORDER BY id DESC LIMIT 1", (device[4], params["timestamp"]))

        if "full" in params:
            placename = reverse_geocode(lat, lon)
        else:
            placename = ""
        
        fuel = -1 #get_last_fuel(device[4])

        devices.append({"gpstype": device[0],
                        "vehicletype": device[1],
                        "name": device[2],
                        "descr": device[3],
                        "id": device[4],
                        "lat": lat,
                        "lon": lon,
                        "timestamp": details[0][2],
                        "speed": details[0][3],
                        "odo": device[5],
                        "fuel": fuel,
                        "placename": placename,
                        "report_every": device[6]})
    return devices


def op_get_device_details(params):
    #require_valid_user(params)

    r = db.query("SELECT lat,lon,timestamp,speed FROM updates" +\
                             " WHERE device = %s AND timestamp < %s " +\
                            "ORDER BY id DESC LIMIT 2880", \
                            (params["id"], params["timestamp"]))

    if len(r) < 2880:

        additional = db.query("SELECT lat,lon,timestamp,speed FROM updates_" + str(params["id"]) +\
                             " WHERE timestamp < %s " +\
                            "ORDER BY id DESC LIMIT 2880", \
                            (params["timestamp"]))
        r = list(r)
        r.extend(list(additional))

    details = []
    stops = []

    waypoints = []

    min_stop = 15
    stop_timer = min_stop

    last_lat = 0
    last_lon = 0

    min_dist_diff = 0.5
    dist_count = 1.0

    count = 0
    #r_max = len(r)

    last_timestamp = 0

    has_at_least_one_valid = False

    lat = -1
    lon = -1

    for detail in r:

        count += 1

        lat = detail[0]
        lon = detail[1]
        #smoothing = detail[4]

        last_timestamp = detail[2] 

        is_stop = False

        dist_diff = distance(lat, lon, last_lat, last_lon)

        waypoints.append({"lat": lat,
                            "lon": lon,
                            "timestamp": detail[2],
                            "speed": detail[3],
                            "timestr": timestr(detail[2])})

        if dist_diff < 0.1:
            stop_timer -= 1
            is_stop = True
        else:

            if stop_timer <= 0 and has_at_least_one_valid:
                stops.append({"when": detail[2],
                    "lat": lat,
                    "lon": lon,
                    "length": (stop_timer - min_stop) * -1,
                    "text": reverse_geocode(lat, lon)})

                details.append({"lat": lat,
                        "lon": lon,
                        "timestamp": detail[2],
                        "speed": detail[3],
                        "smoothed": False,
                        })

            stop_timer = min_stop

        last_lat = lat
        last_lon = lon

        if is_stop:
            continue

        has_at_least_one_valid = True

        dist_count += dist_diff

        if dist_count < min_dist_diff:
            continue
        dist_count = 0

        #if smoothing != "":
        #    points = smoothing.split("|")

        #    plist = points
        #    for pointstr in plist:

        #        p = pointstr.split(",")

        #        details.append({"lat": p[0],
        #                        "lon": p[1],
        #                        "timestamp": detail[2],
        #                        "speed": detail[3],
        #                        "smoothed": True})
        #else:
        details.append({"lat": lat,
                        "lon": lon,
                        "timestamp": detail[2],
                        "speed": detail[3],
                        "smoothed": False})
    if stop_timer <= 0:
        stops.append({"when": 0, "lat": lat, "lon": lon,
                        "length": (stop_timer - min_stop) * -1,
                        "text": reverse_geocode(lat, lon)})

    if lat != -1 and lon != -1:
        details.append({"lat": lat,
                        "lon": lon,
                        "timestamp": detail[2],
                        "speed": detail[3],
                        "smoothed": False})

    return {"stops": stops, "line": details, "device": params["id"], "waypoints": waypoints}




###############################################################################
# Stop/movement reports
###############################################################################
def op_get_movement_report(params):

    te = int(params["timeend"])

    if te < (60 * 60 * 24 * 31):
        te += time.time()

    params["timestamp"] = te

    data = op_get_device_details(params)

    ret = []
    count = 0
    last_region = ""
    last_pos = (0, 0)
    last_time = 0
    stop_timer = 0
    region = "NONE"

    for s in data["waypoints"]:
        count += 1

        this_pos = (s["lat"], s["lon"])

        dist = distance(last_pos[0], last_pos[1], this_pos[0], this_pos[1])

        stop_timer += 1

        if dist < 2:  # and stop_timer < 100:
            continue

        stop_timer = 0

        last_pos = this_pos

        region = reverse_geocode(s["lat"], s["lon"])

        if region.strip() == last_region.strip():
            continue

        s["region"] = region
        s["last_region"] = last_region
        s["length"] = last_time - s["timestamp"]
        s["end_time"] = s["timestamp"] + s["length"]
        s["start_time"] = s["timestamp"]
        s["type"] = "movement"

        last_region = region
        last_time = s["timestamp"]

        #if s["length"] <= 0:
        #    continue  # hacky fix to negative time bug. I'm in india sick and I don't care about fixing this properly

        ret.append(s)

    #append the last one
    if region != "NONE":
        s["region"] = region
        s["length"] = last_time - s["timestamp"]
        s["end_time"] = s["timestamp"] + s["length"]
        s["start_time"] = s["timestamp"]
        s["type"] = "movement"
        ret.append(s)

    ret = ret[::-1][:-1]

    if False:
        stops = data["stops"]

        for s in stops:
            s["timestamp"] = s["when"]
            s["region"] = reverse_geocode(s["lat"], s["lon"])
            s["type"] = "stop"
            s["length"] = s["length"] * 30  # urgh
            s["start_time"] = s["timestamp"]
            s["end_time"] = s["start_time"] + s["length"]

            if s["timestamp"] > 0:
                ret.append(s)

        from operator import itemgetter
        ret = sorted(ret, key=itemgetter('timestamp'))

    return ret





def op_device_setting(params):

    if "value" in params:
        val = params["value"]
        key = params["key"]

        r = db.query("UPDATE devices SET " + key + " = %s WHERE id = %s", (val, params["id"]))
        return True

    else:
        r = db.query("SELECT " + params["key"] + " FROM devices WHERE id = %s", (params["id"]))
        return r[0][0]


def op_get_fuel_limits(params):

    fuel_max = db.query("SELECT fuel FROM updates WHERE device = %s and fuel > 0 ORDER by fuel desc LIMIT 1", (params["id"]))[0][0]
    fuel_min = db.query("SELECT fuel FROM updates WHERE device = %s and fuel > 0 ORDER by fuel LIMIT 1", (params["id"]))[0][0]
    #fuel_first = db.query("SELECT fuel FROM updates WHERE device = %s and fuel > 0 ORDER by id LIMIT 1", (params["id"]))[0][0]

    current = db.query("SELECT fuel_max, fuel_min FROM devices WHERE id = %s", params["id"])[0]

    return {"fuel_max": fuel_max, "fuel_min": fuel_min, "fuel_first": 0, "db_max": current[0], "db_min": current[1]}


###############################################################################
# Update
###############################################################################
def op_del_history(params):
    db.query("DELETE FROM updates WHERE timestamp < %s", time.time() - int(params["before"]))
    return True


def op_add_update(params):

    if "timestamp" not in params:
        params["timestamp"] = str(time.time())

    device = get_device_id_by_imei(params["device"])

    db.query("INSERT INTO updates(device,timestamp,lat,lon,speed,fuel) " +\
        "VALUES(%s,%s,%s,%s,%s,%s)",
            (device, params["timestamp"],
                params["lat"], params["lon"],
                params["speed"], params["fuel"]))

    uid = db.get_last_id()

    db.query("UPDATE devices SET last_update_id = %s WHERE id = %s", (uid, device))

    return True


def op_fix_last_update(params):
    devices = db.query("SELECT id FROM devices", ())

    for d in devices:
        did = d[0]
        m = db.query("SELECT max(id) FROM updates WHERE device = %s", (did))
        db.query("UPDATE devices SET last_update_id = %s WHERE id = %s", (m[0][0], did))

    return True


def op_update_milage(data):
    r = db.query("SELECT lat,lon FROM updates WHERE device=%s ORDER BY id DESC LIMIT 1", (data["id"]))

    if len(r) == 0:
        return 0

    last_lat = r[0][0]
    last_lon = r[0][1]

    dist = distance(last_lat, last_lon, data["lat"], data["lon"])

    if dist > 0.1:
        db.query("UPDATE devices SET odo = odo + %s WHERE id=%s", (dist, data["id"]))

    r = db.query("SELECT odo FROM devices WHERE id=%s", (data["id"]))

    return r[0][0]


def op_import(params):

    data = importer.process(params["data"])

    device = get_device_id_by_imei(data["device"])
    data["id"] = device

    milage = op_update_milage(data)

    data["odo"] = milage
    data["milage"] = milage

    op_add_update(data)
    op_check_notifications(data)

    #op_update_snaps({"id": device, "pending": True})
    #op_update_snaps({"pending": True})

    return {"parsed": data, "status": True}
  

def op_mass_import(params):
    count = 0
    errors = 0
    for line in open(params["file"]).readlines():
      
        print line

        readings = line.split(marker)[1:]

        if len(readings) > 1:
            print len(readings)

        for d in readings:
            try:
                data = importer.process(marker + d)
            except:
                print "**** error"
                errors += 1

            if int(data["timestamp"]) < last:
                continue

            count += 1
            print str(count) + "," + str(errors)

            op_add_update(data)

            #device = get_device_id_by_imei(data["device"])

            #op_update_snaps({"id": device, "pending": True})

            #op_update_snaps({"id": db.get_last_id()})

    return count


def op_mass_fix_fuel(params):
    count = 0

    #db.query("UPDATE updates SET fuel = 0", ())

    devices = {}

    lines = open(params["file"]).readlines()
    for line in lines:
        count += 1

        if line.find("$$") <= 0:
            continue

        start = line.find("$$")

        line = line[start:]
        #print line

        device = -1

        try:
            data = importer.process(line)

            if data["device"] not in devices:
                device = get_device_id_by_imei(data["device"])
                devices[data["device"]] = device
            else:
                device = devices[data["device"]]

        except:
            continue

        #if data["fuel"] > 0:
        if count % 1000 == 0:
            print str(count) + "," + data["timestr"] + "," + str(data["fuel"])

        db.query("UPDATE updates SET fuel = %s WHERE timestamp = %s AND device = %s", 
            (data["fuel"], data["timestamp"], device))

    return count


def get_device_id_by_imei(imei):
    r = db.query("SELECT id FROM devices WHERE imei = %s", (imei))
    return r[0][0]




###############################################################################
# Reports
###############################################################################
def op_get_saved_reports(params):

    uid = require_valid_user(params)

    r = db.query("SELECT id,title FROM reports WHERE owner = %s", (uid))

    ret = []

    for report in r:
        ret.append({"id": report[0], "title": report[1]})

    return ret


def op_save_report(params):

    uid = require_valid_user(params)

    db.query("INSERT INTO reports(owner,attrib,devices,time,title) \
        VALUES(%s,%s,%s,%s,%s)", \
        (uid, params["attrib"], params["devices"], \
            params["time"], params["title"]))

    r = db.query("SELECT id FROM reports ORDER BY id DESC LIMIT 1", ())

    return r[0][0]


def op_get_saved_report(params):
   
    r = db.query("SELECT id,attrib,devices,time FROM reports WHERE id = %s", (params["id"]))

    return op_get_report_data(
        {
        "id": r[0][0],
        "attrib": r[0][1],
        "devices": r[0][2],
        "time": r[0][3]
        })


def op_del_saved_report(params):

    uid = require_valid_user(params)
   
    db.query("DELETE FROM reports WHERE id = %s AND owner = %s", (params["id"], uid))

    return True


def op_export_report_data(params):

    data = op_get_report_data(params)

    ret = ""
    for d in data["data"]:
        for r in d:
            ret += str(r) + ","
        ret += "\n"
    return {"csv": ret}

def timeoffset(t):
    return t + (60 * 60 * 4)


def timeoffset2(t):
    return t - (60 * 60 * 3)

def timestr(t):
    return datetime.datetime.fromtimestamp(timeoffset(int(t))).strftime('%a %d %b %l:%M:%S %p')

def timestr2(t):
    return datetime.datetime.fromtimestamp(timeoffset2(int(t))).strftime('%a %d %b %l:%M:%S %p')

def timestr3(t):
    return datetime.datetime.fromtimestamp(int(t)).strftime('%a %d %b %l:%M:%S %p')


import numpy
def smooth(x,window_len=11,window='hanning'):
    if x.ndim != 1:
        raise ValueError, "smooth only accepts 1 dimension arrays."
    if x.size < window_len:
        raise ValueError, "Input vector needs to be bigger than window size."
    if window_len<3:
        return x
    if not window in ['flat', 'hanning', 'hamming', 'bartlett', 'blackman']:
        raise ValueError, "Window is on of 'flat', 'hanning', 'hamming', 'bartlett', 'blackman'"
    s=numpy.r_[2*x[0]-x[window_len-1::-1],x,2*x[-1]-x[-1:-window_len:-1]]
    if window == 'flat': #moving average
        w=numpy.ones(window_len,'d')
    else:  
        w=eval('numpy.'+window+'(window_len)')
    y=numpy.convolve(w/w.sum(),s,mode='same')
    return y[window_len:-window_len+1]

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')



def op_get_report_data(params):
    #require_valid_user(params)

    attrib = params["attrib"]

    if attrib == "milage":
        return get_milage_data(params)

    if attrib == "uptime":
        return get_uptime_data(params)

    devices = params["devices"].split(",")
    timesplit = params["time"].split("|")
    limit_text = ""
    timeformat = "%Y-%m-%d %H:%M:%S"
    time_end = time.time()
    scale = 1.0
    offset = 0
    cap = 99999999
    unit = ""

    if attrib == "fuel":
        unit = "%"

    if attrib == "speed":
        unit = "km/h"

    if int(timesplit[1]) == 0 or int(timesplit[2]) == 0:
        time_begin = time.time() + int(timesplit[1])
        time_end = time.time() + int(timesplit[2])
    else:
        time_begin = int(timesplit[1])
        time_end = int(timesplit[2])

    time_len = int(time_end) - int(time_begin)
    limit_text = "" + timestr(time_begin) + " to " + timestr(time_end) + ""
    timeformat = "%a %d %b %l:00:00 %p"

    if time_len <= 60 * 60:
        timeformat = "%d %b %l:%M:%S %p"
    elif time_len <= 60 * 60 * 24:
        timeformat = "%d %b %l:00 %p"
    elif time_len <= 60 * 60 * 24 * 7:
        timeformat = "%a %d %b %l:00:00 %p"
    else:
        timeformat = "%a %d %b"

    if attrib == "fuel":
        scale = 1.0
        offset = 0
        cap = 10000
        timeformat = "%d %b %l:00 %p"

    r = db.query("SELECT fuel_max,fuel_min,inverse_fuel FROM devices WHERE id=%s", devices[0])

    max_fuel = r[0][0]
    min_fuel = r[0][1]
    inverse_fuel = 0 #r[0][2] == 1
    chunk_size = 0
    min_rise = 3
    smooth_fuel = True

    if "fuel_max" in params:
        max_fuel = int(params["fuel_max"])

    if "fuel_min" in params:
        min_fuel = int(params["fuel_min"])

    if "chunk_size" in params and int(params["chunk_size"]) != -1:
        chunk_size = int(params["chunk_size"])

    if "inverse_fuel" in params and int(params["inverse_fuel"]) != -1:
        inverse_fuel = int(params["inverse_fuel"])

    if "smooth_fuel" in params and int(params["smooth_fuel"]) != -1:
        smooth_fuel = int(params["smooth_fuel"])

    if "min_rise" in params and int(params["smooth_fuel"]) != -1:
        min_rise = int(params["min_rise"])
    
    if attrib == "fuel" and max_fuel == 0 and min_fuel == 0:
        return {"data": []}

    if attrib not in ["fuel", "speed"]:
        raise APIException("Bad attribute")

    data_array = [["Time"]]
    data = []

    title = ""

    for device in devices:

        device_name = get_device_name(device)
        data_array[0].append(device_name + " " + attrib + " (" + unit + ")")

        title += device_name

        sql = "SELECT " + attrib + ",timestamp,speed FROM updates WHERE device=%s AND timestamp > %s AND timestamp < %s ORDER BY timestamp"


        r = db.query(sql, (device, time_begin, time_end))

        if True:
            sql = "SELECT " + attrib + ",timestamp,speed FROM updates_" + str(device) + " WHERE timestamp > %s AND timestamp < %s ORDER BY timestamp"
            additional = db.query(sql, (time_begin, time_end))
            #r = list(r)
            #r.extend(list(additional))
            additional = list(additional)
            additional.extend(list(r))
            r = additional

        timeseries = []
        last_tstr = ""
        last_val = 0

        for datapoint in r:

            speed = datapoint[2]

            value = datapoint[0]
            value -= offset
            value *= scale

            if value > cap:
                value = cap

            if inverse_fuel and attrib == "fuel":
                value = -(value - 800)
                #value = 1000 + value

                #value -= 1200

                #if speed < 10:
                #    value = 0

                #if value > 1500:
                #    value = 0

                #value = -value

                #value -= 1250;


            timestamp = datapoint[1]

            tstr = datetime.datetime.fromtimestamp(timeoffset(timestamp)).strftime(timeformat)

            if tstr == last_tstr:
                timeseries.append(value)
            else:
                if len(timeseries) > 0:
                    if attrib == "fuel":
                        val = fuel_list(timeseries)
                        if val[1] != 0:
                            data.append(val)
                            last_val = val[1]
                        else:
                            val[1] = last_val
                            data.append(val)
                    else:
                        data.append(max_list(timeseries))
                timeseries = [tstr, value]

            last_tstr = tstr

        if len(timeseries) > 0:
            if attrib == "fuel":
                val = fuel_list(timeseries)
                if val[1] != 0:
                    data.append(val)
                    last_val = val[1]
                else:
                    val[1] = last_val
                    data.append(val)
            else:
                data.append(max_list(timeseries))

    if attrib == "fuel" and chunk_size > 1:
        data2 = []
        count = 0
        avglist = []
        last = -9999
        for i in data:
            count += 1
            avglist.append(i)
            if count % chunk_size == 0:
                data2.append(getMax(avglist))
                avglist = []
        m = getMax(avglist)

        if m[0] != '':
            data2.append(m)
        data = data2

    #try:
    if attrib == "fuel" and smooth_fuel and len(data) > 1:

        data2 = []

        last = -9999

        for d in data:
            data2.append(d)

            if last < d:
                #data2.append([d[0] + " ", d[1]])
                data2.append([d[0].replace(":00 ", ":15 ") + "  ", d[1]])
                data2.append([d[0].replace(":00 ", ":30 ") + "   ", d[1]])
                data2.append([d[0].replace(":00 ", ":45 ") + "    ", d[1]])

            last = d

        a = numpy.array([x[1] for x in data2])
        smoothed = smooth(a, window_len=12)
        #smoothed = savitzky_golay(a, 7, 2, deriv=0, rate=1)
        data = zip([x[0] for x in data2], smoothed)
    #except:
    #    pass

    if attrib == "fuel":
        for i in range(0, len(data)):
            data[i] = [data[i][0], fuel_rescale(data[i][1], min_fuel, max_fuel, 0, 100)]

            if inverse_fuel:
                data[i][1] = 100 - data[i][1]

        #for i in range(0, len(data)):
        #    data[i][1] = fuel_snap(data[i][1])

        for i in range(0, len(data)):
            if data[i][1] < 0:
                data[i][1] = 0
            elif data[i][1] > 100:
                data[i][1] = 100

        last = 0

        for i in range(0, len(data)):
            diff = data[i][1] - last

            if diff < min_rise and diff > 0:
                data[i][1] = last
            else:
                last = data[i][1]

        first_val = 0

        for i in range(0, len(data)):
            if data[i][1] > 0:
                first_val = data[i][1]
                break

        for i in range(0, len(data)):
            if data[i][1] == 0:
                data[i][1] = first_val
            else:
                break

    data_array.extend(data)

    title += " from " + limit_text

    ret = {"data": data_array, "title": attrib, "report_title": title, \
            "attrib": attrib, "devices": device, "time": params["time"],
            "time_begin": time_begin, "time_end": time_end, "unit": unit}

    ret["sql"] = sql

    return ret


def fuel_snap(reading):

    if reading < 0:
        reading = 0

    if reading > 100:
        reading = 100

    return (int(reading) / 10) * 10


def fuel_rescale_ex(vehicle, reading):

    if int(reading) == 0:
        return 0

    r = db.query("SELECT fuel_max,fuel_min FROM devices WHERE id=%s", vehicle)
    max_fuel = r[0][0]
    min_fuel = r[0][1]

    if max_fuel == 0 or min_fuel == 0:
        return 0

    return fuel_rescale(int(reading), int(min_fuel), int(max_fuel), 0, 100)


def fuel_rescale(x, mn, mx, a, b):
    t = (b - a) * (x - mn)
    b = mx - mn

    return (t / b) + a


def get_milage_data(params):
    #require_valid_user(params)

    attrib = params["attrib"]

    device = params["devices"].split(",")[0]
    timesplit = params["time"].split("|")

    if int(timesplit[1]) == 0 or int(timesplit[2]) == 0:
        time_begin = time.time() + int(timesplit[1])
        time_end = time.time() + int(timesplit[2])
    else:
        time_begin = int(timesplit[1])
        time_end = int(timesplit[2])

    time_len = time_end - time_begin

    limit_text = "" + timestr(time_begin) + " to " + timestr(time_end) + ""
    timeformat = "%a %d %b %l:00:00 %p"

    if time_len <= (60 * 60)+10:
        timeformat = "%d %b %l:%M:00 %p"
    elif time_len <= (60 * 60 * 24)+10:
        timeformat = "%d %b %l:00:%p"
    elif time_len <= (60 * 60 * 24 * 7)+10:
        timeformat = "%a %d %b"
    else:
        timeformat = "%a %d %b"

    data_array = [["Time"]]
    data = []

    title = ""

    device_name = get_device_name(device)
    data_array[0].append("kilometers travelled")

    title += device_name

    sql = "SELECT lat,lon,timestamp FROM updates WHERE device=%s AND timestamp > %s AND timestamp < %s ORDER BY timestamp"

    r = db.query(sql, (device, time_begin, time_end))

    if True:
        sql = "SELECT lat,lon,timestamp FROM updates_" + str(device) + " WHERE timestamp > %s AND timestamp < %s ORDER BY timestamp"
        additional = db.query(sql, (time_begin, time_end))
        additional = list(additional)
        additional.extend(list(r))
        r = additional

    timeseries = []
    last_tstr = ""

    last_lat = 0
    last_lon = 0

    for datapoint in r:

        lat = datapoint[0]
        lon = datapoint[1]
        timestamp = datapoint[2]
        value = distance(last_lat, last_lon, lat, lon)

        #if value > 0:
            #printe(str(value))

        #if value > 0 and value < 0.1:
        #    value = 0.25

        if value < 0.05:
            continue

        if last_lat == 0:
            last_lat = lat
            last_lon = lon
            continue

        last_lat = lat
        last_lon = lon

        tstr = datetime.datetime.fromtimestamp(timeoffset(timestamp)).strftime(timeformat)

        if tstr == last_tstr:
            timeseries.append(value)
        else:
            if len(timeseries) > 0:
                data.append(add_list(timeseries))
            timeseries = [tstr, value]

        last_tstr = tstr

    if len(timeseries) > 0:
        data.append(add_list(timeseries))
    data_array.extend(data)

    title += " from " + limit_text

    ret = {"data": data_array, "title": attrib, "report_title": title, \
            "attrib": attrib, "devices": device, "time": params["time"], \
            "unit": "kms", "time_len": time_len}

    return ret


def get_uptime_data(params):
    #require_valid_user(params)

    attrib = params["attrib"]

    device = params["devices"].split(",")[0]
    timesplit = params["time"].split("|")

    if int(timesplit[1]) == 0 or int(timesplit[2]) == 0:
        time_begin = time.time() + int(timesplit[1])
        time_end = time.time() + int(timesplit[2])
    else:
        time_begin = int(timesplit[1])
        time_end = int(timesplit[2])

    time_len = time_end - time_begin

    limit_text = "" + timestr(time_begin) + " to " + timestr(time_end) + ""
    timeformat = "%a %d %b %l:00:00 %p"

    if time_len <= 60 * 60:
        timeformat = "%d %b %l:%M:00 %p"
    elif time_len <= 60 * 60 * 24:
        timeformat = "%d %b %l:%M: %p"
    elif time_len <= 60 * 60 * 24 * 7:
        timeformat = "%a %d %b"
    else:
        timeformat = "%a %d %b"

    data_array = [["Time"]]
    data = []

    title = ""

    device_name = get_device_name(device)
    data_array[0].append("Number of hours operating")

    title += device_name

    sql = "SELECT fuel,timestamp,speed FROM updates WHERE device=%s AND timestamp > %s AND timestamp < %s ORDER BY timestamp"

    r = db.query(sql, (device, time_begin, time_end))

    if True:
        sql = "SELECT fuel,timestamp,speed FROM updates_" + str(device) + " WHERE timestamp > %s AND timestamp < %s ORDER BY timestamp"
        additional = list(db.query(sql, (time_begin, time_end)))
        additional.extend(list(r))
        r = additional


    timeseries = []
    last_tstr = ""

    last_timestamp = 0

    value = 0

    for datapoint in r:

        fuel = datapoint[0]
        timestamp = datapoint[1]
        speed = datapoint[2]

        if speed > 0 or fuel > 0 and last_timestamp != 0:
            value = float(timestamp - last_timestamp) / (60 * 60)
        else:
            value = 0
        last_timestamp = timestamp

        #if value > 0:
            #printe(str(value))

        #if value > 0 and value < 0.1:
        #    value = 0.25

        tstr = datetime.datetime.fromtimestamp(timeoffset(timestamp)).strftime(timeformat)

        if tstr == last_tstr:
            timeseries.append(value)
        else:
            if len(timeseries) > 0:
                data.append(add_list_float(timeseries))
            timeseries = [tstr, value]
            value = 0

        last_tstr = tstr

    if len(timeseries) > 0:
        data.append(add_list(timeseries))
    data_array.extend(data)

    title += " from " + limit_text

    ret = {"data": data_array, "title": "Hours log", "report_title": title, \
            "attrib": attrib, "devices": device, "time": params["time"], \
            "unit": "kms"}

    return ret


def average_list(l):

    if len(l) == 0:
        return []

    count = 0
    avg = 0

    for i in l[1:]:
        if i > 0:
            count += 1
        avg += i

    if count > 0:
        avg /= count

    if avg < 0:
        avg = 0

    return [l[0], int(avg)]


def average_list2(l):

    if len(l) == 0:
        return 0

    count = 0
    avg = 0

    for i in l:
        if i > 0:
            count += 1
        avg += i

    if count > 0:
        avg /= count

    if avg < 0:
        avg = 0

    return avg


def getMedian(numericValues):
    theValues = sorted(numericValues)

    if len(theValues) % 2 == 1:
        return theValues[(len(theValues) + 1) / 2 - 1]
    else:
        lower = theValues[len(theValues) / 2 - 1]
        upper = theValues[len(theValues) / 2]

        return (float(lower + upper)) / 2


def getMax(vals):
    if len(vals) == 0:
        return ["",0]
    theValues = sorted(vals)
    return theValues[-1]


def fuel_list(l):
    #return [l[0], getMedian(l[1:])]
    return average_list(l)
    #return min_list(l)


def max_list(l):

    if len(l) == 0:
        return []

    maxnum = 0

    for i in l[1:]:
       if i > maxnum:
            maxnum = i

    return [l[0], int(maxnum)]


def add_list(l):
    r = 0
    for v in l[1:]:
        r += v
    return [l[0], int(r)]

def add_list_float(l):
    r = 0
    for v in l[1:]:
        r += v

    rounded = "%.2f" % r

    return [l[0], float(rounded)]


def distance(lat1, lon1, lat2, lon2):
    radius = 6371 # km

    dlat = math.radians(lat2-lat1)
    dlon = math.radians(lon2-lon1)
    a = math.sin(dlat/2) * math.sin(dlat/2) + math.cos(math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon/2) * math.sin(dlon/2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
    d = radius * c

    return d


def midPoint(lat1, lon1, lat2, lon2):

    dLon = math.radians(lon2 - lon1)

    #convert to radians
    lat1 = math.radians(lat1)
    lat2 = math.radians(lat2)
    lon1 = math.radians(lon1)

    Bx = math.cos(lat2) * math.cos(dLon)
    By = math.cos(lat2) * math.sin(dLon)
    lat3 = math.atan2(math.sin(lat1) + math.sin(lat2), math.sqrt((math.cos(lat1) + Bx) * (math.cos(lat1) + Bx) + By * By))
    lon3 = lon1 + math.atan2(By, math.cos(lat1) + Bx)

    #print out in degrees
    return (math.degrees(lat3), math.degrees(lon3))


def midPoint2(a, b):
    return midPoint(a[0], a[1], b[0], b[1])


###############################################################################
# Notifications
###############################################################################
def send_sms(number, message):
    url = "http://rest.nexmo.com/sms/json?" +\
            "username=a1a1bb00&" +\
            "password=40521011&" +\
            "from=SeyTrax&" +\
            "to=" + number + "&" +\
            "text=" + urllib.quote_plus(message)

    response = urllib.urlopen(url).read()

    data = json.loads(response)

    return data


def op_add_notification(params):
    uid = require_valid_user(params)

    notifystr = ""

    if params["email"] == "true":
        notifystr += "e"

    if params["sms"] == "true":
        notifystr += "s"

    db.query("INSERT INTO notifications(owner,vehicleid,attributeid,attribvalue,notify, last_trigger) \
        VALUES(%s,%s,%s,%s,%s,0)", \
        (uid, params["vehicle"], params["attrib"], \
            params["val"], notifystr))

    return True


def op_del_notification(params):
    uid = require_valid_user(params)

    db.query("DELETE FROM notifications WHERE id = %s AND owner = %s",
        (params["id"], uid))

    return True


def op_get_notification_triggers(params):
    uid = require_valid_user(params)

    r = db.query("SELECT id,vehicleid,attributeid,attribvalue,notify \
        FROM notifications WHERE owner = %s", (uid))

    ret = []

    for n in r:
        ret.append({"id": n[0], "vehicleid": n[1], "attrib": n[2], "value": n[3], "notify": n[4]})

    return ret


#######################
# Triggers
######################
def check_speed_trigger(data, attribvalue):
    if int(data["speed"]) > int(attribvalue):
        return True, "speed was above " + str(attribvalue) + "km/h"
    return False, ""


def check_fuel_trigger(data, attribvalue):
    f = fuel_rescale_ex(data["id"], data["fuel"])
    if f < int(attribvalue):
        return True, "fuel was below " + str(attribvalue) + "%"
    return False, ""


def check_outside_geofence(data, attribvalue):
    name, b = check_geofence(data, attribvalue)
    return b == False, "was outside the area '" + name + "'"


def check_inside_geofence(data, attribvalue):
    name, b = check_geofence(data, attribvalue)
    return b == True, "was inside the area '" + name + "'"


def check_milage(data, attribvalue):
    if int(data["odo"]) > int(attribvalue):
        return True, "Milage was above " + str(attribvalue) + "km/h"
    return False, ""


def check_geofence(data, attribvalue):
    s = attribvalue.split("|")
    name = s[0]
    pointstr = s[1:]

    polygon = []

    for latlon in pointstr:
        l = latlon.split(",")

        if len(l) != 2:
            continue

        #printe(str(l))

        if len(l[0]) > 0 and len(l[1]) > 0:
            point = [float(l[0]), float(l[1])]
            polygon.append(point)

    vehiclept = [float(data["lat"]), float(data["lon"])]

    return name, point_inside_polygon(vehiclept, polygon)


def point_inside_polygon(point, polygon):

    angle = 0

    for i in range(0, len(polygon)):
        point1_lat = polygon[i][0] - point[0]
        point1_lon = polygon[i][1] - point[1]

        point2_lat = polygon[(i + 1) % len(polygon)][0] - point[0]
        point2_lon = polygon[(i + 1) % len(polygon)][1] - point[1]

        angle += Angle2D(point1_lat, point1_lon, point2_lat, point2_lon)

    if math.fabs(angle) < math.pi:
        return False
    else:
        return True


def Angle2D(x1, y1, x2, y2):

    theta1 = math.atan2(y1, x1)
    theta2 = math.atan2(y2, x2)
    dtheta = theta2 - theta1

    while (dtheta > math.pi):
        dtheta -= (math.pi * 2)
    while (dtheta < -math.pi):
        dtheta += (math.pi * 2)

    return dtheta


def send_notification(vid, text, notify, owner):

    #get the vehicle name
    r = db.query("SELECT name FROM devices WHERE id=%s", (vid))

    vname = r[0][0]

    #always add to web
    db.query("INSERT INTO `notification_history`( `message`, `when`, \
         `unread`, `owner`) VALUES(%s,%s,%s,%s)", (vname + ": " + text, time.time(), 1, owner))

    if notify.find("e") >= 0:
        send_notify_email(owner, vname + " " + text)

    if notify.find("s") >= 0:
        send_notify_sms(owner, vname + " " + text)

    #if notify.find("s") >= 0:
    #    print "sms"


def send_notify_sms(uid, text):

    r = db.query("SELECT phone FROM users WHERE id=%s", (uid))
    user_phone = r[0][0]

    if user_phone == "":
        return

    send_sms(user_phone, text)


def op_email_test(params):
    send_notify_email(6, "Sent a test email")


def send_notify_email(uid, text):

    r = db.query("SELECT email FROM users WHERE id=%s", (uid))
    user_email = r[0][0]

    if user_email == "test@seytrack.com":
        user_email = "paul@bieh.net"

    import smtplib

    SMTP_SERVER = 'smtp.gmail.com'
    SMTP_PORT = 587

    sender = 'paul@bieh.net'
    recipient = user_email
    subject = '[SeyTrax] ' + text
    body = "This is an automated notification from SeyTrax<br>" +\
            "<br><h2>On " + timestr(time.time()) + ", " + text + "</h2><br><br><hr>" +\
            "(To edit your notification settings, log in to SeyTrax at <a href=\"http://seytrax.com\">seytrax.com</a>)"

    headers = ["From: " + sender,
               "Subject: " + subject,
               "To: " + recipient,
               "MIME-Version: 1.0",
               "Content-Type: text/html"]
    headers = "\r\n".join(headers)

    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)

    session.ehlo()
    session.starttls()
    session.ehlo
    session.login(sender, "tr5aswou")

    session.sendmail(sender, recipient, headers + "\r\n\r\n" + body)
    session.quit()


def get_vehicle_owner(vid):
    r = db.query("SELECT owner FROM devices WHERE id=%s", (vid))
    return r[0][0]


def op_check_notifications(params):

    uid = get_vehicle_owner(params["id"])

    triggers = db.query("SELECT id,vehicleid,attributeid,attribvalue,notify,last_trigger,owner \
        FROM notifications WHERE (vehicleid = %s OR vehicleid = 0) AND owner = %s", (params["id"], uid))

    for trigger in triggers:
        #vid = trigger[1]
        attribid = trigger[2]
        attribvalue = trigger[3]
        notify = trigger[4]
        last = trigger[5]
        owner = trigger[6]

        if last > time.time() - (60 * 60 * 24):
            #last triggered less than 24h ago
            continue

        has_triggered = False
        text = ""

        if attribid == 1:
            has_triggered, text = check_speed_trigger(params, attribvalue)
        elif attribid == 2:
            has_triggered, text = check_fuel_trigger(params, attribvalue)
        elif attribid == 3:
            has_triggered, text = check_outside_geofence(params, attribvalue)
        elif attribid == 4:
            has_triggered, text = check_inside_geofence(params, attribvalue)
        elif attribid == 5:
            has_triggered, text = check_milage(params, attribvalue)

        if has_triggered:
            #printe("trigger!")
            send_notification(params["id"], text, notify, owner)

            db.query("UPDATE notifications SET last_trigger = %s WHERE id = %s", (time.time(), trigger[0]))

    return True


def op_get_notification_history(params):
    uid = require_valid_user(params)

    limit = 10

    if "limit" in params:
        limit = int(params["limit"])

    r = db.query("SELECT id,`message`,`when`,unread FROM notification_history WHERE owner = %s ORDER BY id DESC LIMIT %s", (uid, limit))

    ret = []

    for note in r:
        n = {"text": note[1], "when": note[2], "unread": note[3]}
        ret.append(n)
    return ret


def op_mark_notifications_read(params):
    uid = require_valid_user(params)

    db.query("UPDATE notification_history SET unread = 0 WHERE owner = %s", (uid))
    return None




###############################################################################
# Road snap points
###############################################################################
def op_add_snap(args):
    f = open("snaps.txt", "a")

    s = args["lat"] + "," + args["lon"] + "\n"

    f.write(s)
    f.close()

    return True


def op_get_snaps(args):
    f = open("snaps.txt")

    ret = []

    for line in f.readlines():
        s = line.split(",")
        ret.append({"lat": s[0], "lon": s[1]})

    f.close()

    return ret


def find_closest(point, plist):
    cpoint = None
    cdist = 99999999

    for snap in plist:
        dist = distance(snap[0], snap[1], point[0], point[1])

        if dist < cdist and dist < 0.6:
            cdist = dist
            cpoint = snap

    return cpoint


def c(pt):
    return str(pt[0]) + "," + str(pt[1])


def op_update_snaps(args):

    #print "Loading snaps"

    f = open("snaps.txt")

    snaps = []

    for line in f.readlines():
        s = line.split(",")
        snaps.append((float(s[0]), float(s[1])))

    f.close()

    #print "loaded"

    where = ""

    if "id" in args:
        where = " WHERE device = " + str(args["id"]) + " ORDER by id DESC LIMIT 1440"

    if "pending" in args:
        where = " WHERE device = " + str(args["id"]) + " ORDER by id DESC LIMIT 4"

    sql = "SELECT id,lat,lon FROM updates" + where

    #print sql

    r = db.query(sql, ())

    counter = 0

    last = (0, 0)

    for u in r:
        counter += 1
        try:
            start = find_closest(last, snaps)
            end = find_closest((u[1], u[2]), snaps)

            last = (u[1], u[2])

            middle = find_closest(midPoint2(start, end), snaps)
            postmiddle = find_closest(midPoint2(middle, end), snaps)
            premiddle = find_closest(midPoint2(start, middle), snaps)

            #smoothstr = c(start) + "|"
            smoothstr = c(premiddle) + "|"
            smoothstr += c(middle) + "|"
            smoothstr += c(postmiddle) + "|"
            smoothstr += c(end)

            if "verbose" in args:
                print "[x] " + str(u[0]) + " -- " + str(counter) + "/" + str(len(r)) + " - " + smoothstr

            sql = "UPDATE updates SET smoothing=%s WHERE id=%s"
            db.query(sql, (smoothstr, u[0]))
        except:
            if "verbose" in args:
                print "[ ] " + str(u[0]) + " -- " + str(counter) + "/" + str(len(r))

    return True


def op_get_geocoder_cache(params):
    geocode_cache = open("geocoder.txt")

    ret = []

    for line in geocode_cache.readlines():
        a = line.split(",")
        glat = float(a[0])
        glon = float(a[1])

        ret.append({"lat": glat, "lon": glon, "name": a[2].strip()})
    geocode_cache.close()
    return ret


def op_update_geocoder_cache(params):
    geocode_cache = open("geocoder.txt")

    geocode_new = ""
    found = False

    lat = float(params["lat"])
    lon = float(params["lon"])

    for line in geocode_cache.readlines():
        a = line.split(",")
        glat = float(a[0])
        glon = float(a[1])

        if distance(lat, lon, glat, glon) < 0.01:
            found = True
            continue
        geocode_new += line

    geocode_cache.close()

    if params["name"] != 'DELETE':
        geocode_new += str(lat) + "," + str(lon) + "," + params["name"].encode("utf8") + "\n"

    geocode_cache = open("geocoder.txt", "w")
    geocode_cache.write(geocode_new)
    geocode_cache.close()

    return True


def op_populate_geocoder(params):

    for d in db.query("SELECT id,owner,name FROM devices", ()):
        send_daily_report(d[0], d[1], d[2], False)


def reverse_geocode(lat, lon, recurse=0):

    #First try it with the regions
    geocode_cache = open("geocoder_regions.txt")

    for line in geocode_cache.readlines():
        a = line.split("|")
        name, inside = check_geofence({"lat": lat, "lon": lon}, line[line.find("|"):])

        if inside:
            return a[0]

    geocode_cache.close()

    geocode_cache = open("geocoder.txt")

    for line in geocode_cache.readlines():
        a = line.split(",")
        glat = float(a[0])
        glon = float(a[1])

        if distance(lat, lon, glat, glon) < 0.5:
            geocode_cache.close()
            return a[2].strip()
    geocode_cache.close()

    if recurse > 1:
        return "(Over query limit)"

    time.sleep(0.1)  # lolgoogle


    url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + str(lat) + "," + str(lon) + "&sensor=true"
    r = urllib.urlopen(url).read()
    j = json.loads(r)

    #print url

    if j["status"] == "OVER_QUERY_LIMIT":
        #print "over query limit (" + str(recurse) + ")"
        #time.sleep(2)
        #return reverse_geocode(lat, lon, recurse + 1)
        return "(Geocoder failed: Over query limit)"


    s = ""
    for i in j["results"]:
        for ac in i["address_components"]:
            ln = ac["long_name"]

            if s.find(ln) >= 0:
                continue

            if "administrative_area_level_1" in ac["types"]:
                s += ln + ", "
            if "route" in ac["types"] and ln != "5th June Ave":
                s += ln + ", "
    ret =  s.strip()[:-1]

    geocode_cache = open("geocoder.txt", "a+")
    geocode_cache.write(str(lat) + "," + str(lon) + "," + ret.encode("utf8") + "\n")
    geocode_cache.close()

    #print "cached"

    return ret

    #return j["results"]

def op_update_history_tables(params):
    devs = db.query("SELECT id,name FROM devices", ())

    for d in devs:
        did = d[0]
        dname = d[1]

        db.query("\
                CREATE TABLE IF NOT EXISTS `updates_" + str(did) + "` (\
                  `id` int(11) NOT NULL AUTO_INCREMENT,\
                  `device` int(11) NOT NULL,\
                  `timestamp` int(11) NOT NULL,\
                  `lat` float NOT NULL,\
                  `lon` float NOT NULL,\
                  `speed` float NOT NULL,\
                  `fuel` float NOT NULL,\
                  PRIMARY KEY (`id`),\
                  KEY `timestamp` (`timestamp`),\
                  KEY `device` (`device`)\
                )", ())

        ts = time.time() - (60 * 60 * 24)
        monthago = time.time() - (60 * 60 * 24 * 30)

        print dname + " (1/3)"

        db.query("INSERT INTO updates_" + str(did) + " select * FROM updates WHERE timestamp < %s AND device = %s", (ts, did))

        print dname + " (2/3)"

        db.query("DELETE FROM updates WHERE timestamp < %s AND device = %s", (ts, did))

        print dname + " (3/3)"

        db.query("DELETE FROM updates_" + str(did) + " WHERE timestamp < %s", (monthago))

    return True



###############################################################################
# Geocoder region stuff
###############################################################################
def op_add_geocoder_region(params):
    geocode_cache = open("geocoder_regions.txt", "a+")

    line = params["name"] + "|" + params["points"] + "\n"

    geocode_cache.write(line)
    geocode_cache.close()

    return True

def op_del_geocoder_region(params):
    geocode_cache = open("geocoder_regions.txt")

    out = ""

    for line in geocode_cache.readlines():
        if line.find(params["name"]) == 0:
            continue
        out += line.strip() + "\n"

    geocode_cache.close()

    geocode_cache = open("geocoder_regions.txt", "w")
    geocode_cache.write(out)
    geocode_cache.close()

    return True

def op_rename_geocoder_region(params):
    geocode_cache = open("geocoder_regions.txt")

    out = ""

    for line in geocode_cache.readlines():
        if line.find(params["old"]) == 0:
            out += line.replace(params["old"], params["new"]).strip() + "\n"
            continue
        out += line.strip() + "\n"

    geocode_cache.close()

    geocode_cache = open("geocoder_regions.txt", "w")
    geocode_cache.write(out)
    geocode_cache.close()

    return True


def op_get_geocoder_regions(params):
    geocode_cache = open("geocoder_regions.txt")

    ret = []

    for line in geocode_cache.readlines():
        a = line.partition("|")
        ret.append({"points": a[2].split("|")[:-1], "name": a[0]})
    geocode_cache.close()
    return ret


###############################################################################
# Mixpanel integration
###############################################################################
def track(event, properties=None):
    """
    A simple function for asynchronously logging to the mixpanel.com API.
    This function requires `curl` and Python version 2.4 or higher.

    @param event: The overall event/category you would like to log this data under
    @param properties: A dictionary of key-value pairs that describe the event
                       See http://mixpanel.com/api/ for further detail.
    @return Instance of L{subprocess.Popen}
    """
    if properties == None:
        properties = {}
    
    # XXX: Be sure to change this!
    token = "3dcc4929cf9c46de240e3f0fac06ce11"
    
    if "token" not in properties:
        properties["token"] = token

    properties["ip"] = cgi.escape(os.environ["REMOTE_ADDR"])

    params = {"event": event, "properties": properties}
    data = base64.b64encode(simplejson.dumps(params))
    request = "http://api.mixpanel.com/track/?data=" + data
    return subprocess.Popen(("curl",request), stderr=subprocess.PIPE,
        stdout=subprocess.PIPE)

###############################################################################
# Actual entry point
###############################################################################
if __name__ == "__main__":
    try:
        db.connect()
        result = main()

        if isinstance(result["result"], dict) and "csv" in result["result"]:
            print "Content-Type: text/csv"
            print "Content-Disposition: attachment;filename=report.csv\r\n"
            print result["result"]["csv"]

        else:

            print "Content-Type: text/plain\r\n"
            print json.dumps(result, sort_keys=True, indent=4)
        #print result
    except Exception, e:
        #cgitb.handler()
        print "Content-Type: text/plain\r\n"

        tb = traceback.format_exc()

        tbarr = []

        for i in tb.split("\n"):
            tbarr.append(i)

        out = json.dumps({"status": False, "result": None,
            "error": "internal server error",
            "traceback": tbarr}, sort_keys=True, indent=4)

        print out.replace("\\n", "\n")

        printe(out)
    finally:
        db.disconnect()
