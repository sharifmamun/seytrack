
###############################################################################
# Importer
###############################################################################

import datetime
import time


###############################################################################
# Util functions
###############################################################################
def dms_to_dec(dms, ll):
    if ll == 'lat':
        deg = dms[0:2]
        min = dms[2:8]
        sec = 0

    if ll == 'lon':
        deg = dms[0:3]
        min = dms[3:8]
        sec = 0

    return float(deg) + (((float(min) * 60) + (float(sec))) / 3600)


def parse_gpmc(gpmc):
    ret = {}

    s = gpmc.split(",")

    ret = {}
    
    #ret["gpmc"] = gpmc

    ret["lat"] = dms_to_dec(s[3], "lat")
    ret["lat_direction"] = s[4]

    #ret["lon_dms"] = s[9]
    ret["lon"] = dms_to_dec(s[5], "lon")
    ret["lon_direction"] = s[6]

    if ret["lat_direction"] == "S":
        ret["lat"] = -ret["lat"]

    if ret["lon_direction"] == "W":
        ret["lon"] = -ret["lon"]

    ret["speed"] = float(s[7]) * 1.852
    ret["knots"] = s[7]
    ret["heading"] = s[8]

    return ret


###############################################################################
# Process a tk10x string
###############################################################################
def process_tk(data):
    #print data
    s = data.split(",")

    ret = {}
    ret["data"] = data
    ret["device"] = s[0].replace("imei:", "")
    #ret["lat_dms"] = s[7]
    ret["lat"] = dms_to_dec(s[7], "lat")
    ret["lat_direction"] = s[8]

    #ret["lon_dms"] = s[9]
    ret["lon"] = dms_to_dec(s[9], "lon")
    ret["lon_direction"] = s[10]

    if ret["lat_direction"] == "S":
        ret["lat"] = -ret["lat"]

    if ret["lon_direction"] == "W":
        ret["lon"] = -ret["lon"]

    ret["speed"] = float(s[11]) * 1.852
    ret["heading"] = s[12]
    #ret["numsat"] = s[18]

    #1207052106
    ts = s[2]
    dt = datetime.datetime(int("20" + ts[0:2]), int(ts[2:4]), int(ts[4:6]), int(ts[6:8]), int(ts[8:10]))

    ret["timestamp"] = time.mktime(dt.timetuple())
    
    #unknown fuel
    ret["fuel"] = -1
    ret["type"] = "tk"

    return ret


###############################################################################
# Process the avl102 string
###############################################################################
last_fuel = 0

def process_avl(data):

    ret = {}
    ret["type"] = "avl"

    s = data.split("|")

    ret["device"] = s[0][4:]

    ts = s[6]
    dt = datetime.datetime(int(ts[0:4]), int(ts[4:6]), int(ts[6:8]), int(ts[8:10]), int(ts[10:12]))

    ret["adc"] = s[-6]

    adc = ret["adc"]

    adc1 = adc[:4]
    adc2 = adc[4:]

    ret["adc1"] = 800 - float(adc1)
    ret["adc2"] = 800 - float(adc2)

    global last_fuel

    if adc1 == "0000":
        ret["fuel"] = 0  # last_fuel
    else:
        ret["fuel"] = ret["adc1"]
        #last_fuel = ret["adc1"]

    #print adc

    ret["timestamp"] = time.mktime(dt.timetuple())
    ret["speed"] = 0
    ret["heading"] = 0
    ret["timestr"] = datetime.datetime.fromtimestamp(ret["timestamp"]).strftime("%d %b %l:%M:%S %p")


    gpmc = parse_gpmc(s[1])

    ret.update(gpmc)

    return ret


###############################################################################
# Main dispatcher
###############################################################################
def process(data):
    #print data
    if data.find("$$") == 0:
        ret = process_avl(data)
    else:
        ret = process_tk(data)

    ret["google"] = 'http://maps.google.com/maps?q=' +\
                     str(ret['lat']) + ',' + str(ret['lon'])

    return ret

if __name__ == "__main__":
    for line in open("tmp.txt"):
        l = line.split("\t")
        if len(l) > 1:
            s = process(l[1])

            if s["speed"] > 70:
                print s["timestr"]
                print s["speed"]
                print s["knots"]
                print

#print process("$$B9359772036069681|AA$GPRMC,082526.000,A,0439.5027,S,05529.3023,E,37.80,281.99,300712,,,A*4F|01.6|00.7|01.4|000000100000|20120730082526|14091375|00000000|000100C5|0000|0.6974|0344|B739")
#print process("$$B2359772036069681|AA$GPRMC,122440.000,A,0443.4406,S,05531.4232,E,0.00,,130712,,,A*6A|02.6|01.1|02.3|000000000000|20120713122440|03790000|00000000|00010100|0000|0.0000|0116|2CEE")
