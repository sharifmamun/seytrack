
###############################################################################
# Database abstraction
###############################################################################

import MySQLdb as mdb
import config
import time
import sys


def printe(str):
    sys.stderr.write(str + '\n')

con = None


def connect():
    global con
    con = mdb.connect(config.DB_HOST, config.DB_USER,
              config.DB_PASSWORD, config.DB_DATABASE)


def query(sql, params):
    start = time.time()
    global con
    cur = con.cursor()
    cur.execute(sql, params)
    r = cur.fetchall()
    end = time.time()
    length = end - start

    if length > 5:
        printe("SLOW QUERY: " + str(length) + " - " + sql + ", " + str(params))

    return r



def disconnect():
    global con
    if con:
        con.close()


def get_last_id():
    global con
    return con.insert_id()
