#!/usr/bin/python

import smtplib
import urllib
import json

def pad(s):
    l = 40

    if len(s) > l:
        return s[:l - 3] + "..."

    for i in range(len(s), l):
        s += " "
    return s

def main():
    devices = json.loads(urllib.urlopen("http://seytrax.com/api/api.py?op=device_list_ex").read())

    text = "The following devices have not updated in the last hour:<br><pre><ul>\n"

    num_broken = 0

    for d in devices["result"]:
        if d["last_update_diff"] > 60 * 60:
            text += " <li> " + pad(d["name"] + " - " + d["descr"]) + " " + pad("(" + d["owner"]) + ") "
            text += " last updated " + str(int(d["last_update_diff"] / (60 * 60))) + " hours ago " +\
             " (<a href=\"http://seytrax.com/admin/reset.php?id=" + str(d["id"]) + "\">reset</a>)\n"
            text += " </li>"
            num_broken += 1

    text += "</ul></pre>(" + str(num_broken) + " broken)"

    if num_broken > 0:
        send_email("herald@seytrax.com", text)

def send_email(to, body):

    SMTP_SERVER = 'smtp.gmail.com'
    SMTP_PORT = 587

    sender = 'paul@bieh.net'
    recipient = to
    subject = '[SeyTrax] Dead device notification'

    headers = ["From: " + sender,
               "Subject: " + subject,
               "To: " + recipient,
               "MIME-Version: 1.0",
               "Content-Type: text/html"]
    headers = "\r\n".join(headers)

    session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)

    session.ehlo()
    session.starttls()
    session.ehlo
    session.login(sender, "tr5aswou")

    session.sendmail(sender, recipient, headers + "\r\n\r\n" + body)
    session.quit()


main()
