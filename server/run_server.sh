#!/bin/bash
echo "Running server"
until ./server.py; do
    echo "Server terminated with exit code $?.  Respawning in 1 second..." >&2
    sleep 1
done
