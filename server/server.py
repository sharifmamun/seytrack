#!/usr/bin/python
# Copyright (c) Twisted Matrix Laboratories.
# See LICENSE for details.

from twisted.internet import reactor, protocol
import json, datetime, time, urllib, subprocess
import time
import traceback

import sys

import db
import api

start_time = time.time()
exit_time = time.time() + (60 * 60)

log = open("data.log", "a+")

devices = []


def write_log(s, num, count, time_len):

    now = datetime.datetime.now()
    log.write(now.strftime("%Y-%m-%d %H:%M") + "(" + str(time.time()) + "):\t" + s + "\n")

    split = s.split("|")
    imei = split[0][4:]

    if imei not in devices:
        print "\n*** NEW DEVICE: " + imei + " ***\n"
        devices.append(imei)

    print now.strftime("%Y-%m-%d %H:%M") + ":\t" + imei + " (" + str(count) + "/" + str(num - 1) + ")" + ", in " + str(time_len * 1000) + " ms"


def send_to_api(data):
    #a = urllib.urlopen("http://seytrack.com/api/api.py?op=import&data=" + data)
    #response = a.read()

    start_time = time.time()

    #response = subprocess.check_output(["../api/api.py", "op=import&data=" + data])
    response = api.op_import({"data": data})

    diff = time.time() - start_time

    if response["status"] != True:
        print "\n*** FAILED: http://seytrack.com/api/api.py?op=import&data=" + data + "\n"
        #send_alert("Failed to import (API returned fail)")
        print response

    return diff


class Echo(protocol.Protocol):
    """This is just about the simplest possible protocol"""

    def dataReceived(self, data):
        "As soon as any data is received, write it back."

        #print data

        if data.find("##,imei:") >= 0:
            self.transport.write("LOAD")
            #print "SENT LOAD: " + data
        elif data.find("imei") < 0:
            self.transport.write("ON")
            #print "SENT ON: " + data

        split = data.split("\n")

        count = 1

        for a in split:

            if len(a) >= 2:
           
                #print data
                time_len = 0
                try:
                    time_len = send_to_api(a)
                except Exception, e:
                    print "*** Error sending to API: " + str(e)
                    print "Data was: " + a
                    print traceback.format_exc()
                    print
                    #send_alert(str(e))
                write_log(a, len(split), count, time_len)
                count += 1


        if time.time() > exit_time:
            print "terminating on schedule"
            reactor.stop()


def send_sms(number, message):
    url = "http://rest.nexmo.com/sms/json?" +\
            "username=a1a1bb00&" +\
            "password=40521011&" +\
            "from=SeyTraxAlert&" +\
            "to=" + number + "&" +\
            "text=" + urllib.quote_plus(message)

    response = urllib.urlopen(url).read()

    data = json.loads(response)

    return data



def send_alert(str):
    send_sms("2482527574", str)

def main():
    print "Listening on port 12345"

    db.connect()

    """This runs the protocol on port 8000"""
    factory = protocol.ServerFactory()
    factory.protocol = Echo
    reactor.listenTCP(12345, factory)
    reactor.run()
    sys.exit(1)

# this only runs if the module was *not* imported
if __name__ == '__main__':
    main()
