<?php

require("utils.php");

if(isset($_REQUEST["delid"])){
	echo "Deleting ".$_REQUEST["delid"];

	$api = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=user_del&id=".$_REQUEST["delid"]));
}
?>


<html>
<head>
	<title>SeyTrackAdmin | list users</title>
</head>

<script type="text/javascript">

function del_user(uid){
	if(confirm("Really delete user?")){
		window.location = "list_users.php?delid=" + uid;
	}
}
</script>
<body>
	<h1><a href="index.php">SeyTrackAdmin</a> | list users</h1>
	<hr>
	
	<table border="1" cellpadding="10">

<?php
	require("utils.php");

	$users = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=user_list"));

	foreach ($users->result as $key => $value) {
		echo "<tr><td>".$value->name."</td><td>".$value->email."</td>";
		echo "<td> id = ".$value->id."</td><td> type = ".$value->usertype."</td>";
		echo "<td> alias = ".$value->useralias."</td>";
		echo "<td><a href=\"edit_user.php?id=".$value->id."\">edit user</a>,  <a href=\"javascript:del_user('".$value->id."');\">Delete</a></td></tr>";
	}

?>
	</table>
</body>
</html>