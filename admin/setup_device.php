<html>
<head>
	<title>SeyTrackAdmin | Set up device</title>
</head>
<body>
	<h1><a href="index.php">SeyTrackAdmin</a> | Set up device</h1>
	This sends SMS commands to a device to configure its software. See the links on the <a href="index.php">index</a> page for link to the full manual reference of commands. 
	<hr>

<?php

	$send = "";
	
	if(isset($_POST["send1"])){
		$send = $_POST["string1"];
	}
	if(isset($_REQUEST["send2"])){
		$send = $_REQUEST["string2"];
	}
	if(isset($_REQUEST["send3"])){
		$send = $_REQUEST["string3"];
	}
	if(isset($_REQUEST["send4"])){
		$send = $_REQUEST["string4"];
	}
	if(isset($_REQUEST["send5"])){
		$send = $_REQUEST["string5"];
	}
	if(isset($_REQUEST["send6"])){
		$send = $_REQUEST["string6"];
	}
	if(isset($_REQUEST["send7"])){
		$send = $_REQUEST["string7"];
	}

	if(strlen($send) > 0){
		$number = $_REQUEST["number"];

		$url = "http://rest.nexmo.com/sms/json?" .
            "username=a1a1bb00&" .
            "password=40521011&" .
            "from=".$_REQUEST["dest_number"]."&" .
            "to=" . $number . "&" .
            "text=" . urlencode($send);

        $response = file_get_contents($url);

		echo "sent: " . $send . " to " . $number."<br>";
		echo "Response from <a href=\"".$url."\">API</a> was: ".$response."<br><br><hr><br><br>";
	}

?>
<br>
	<form action="setup_device.php" method="post">
		Device phone number: <input type="text" name="number" value="<?php echo $_REQUEST["number"]; ?>"> (ie: 2482527574)<br>
		Your phone number:&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" name="dest_number" value="<?php echo $_REQUEST["dest_number"]; ?>">
		<br>
		<i>(note: make sure you've included the 248 country code!)</i>

		<br><br>
	</tr>

		<table>
		
	<tr>
		<td>#1</td>
		<td><input type="text" name="string1" readonly="true" value="*000000,011,internet,,#"></td>
		<td><input type="submit" name="send1" value="Send #1"></td><td> (set APN to 'internet')</td>
	</tr>
	<tr>
		<td>#2</td>
		<td><input type="text" name="string2" readonly="true" value="*000000,015,0,54.243.41.201,12345#"></td>
		<td><input type="submit" name="send2" value="Send #2"></td><td>(point it to the seytrack internet server)</td>
	</tr>
	<tr>
		<td>#3</td>
		<td><input type="text" name="string3" readonly="true" value="*000000,016,1#"></td>
		<td><input type="submit" name="send3" value="Send #3"></td><td> (send updates over GPRS)</td>
	</tr>
	<tr>
		<td>#4</td>
		<td><input type="text" name="string4" readonly="true" value="*000000,018,30,999#"></td>
		<td><input type="submit" name="send4" value="Send #4"></td><td>(send every 30 seconds, forever)</td>
	</tr>
		<td>#5</td>
		<td><input type="text" name="string5" readonly="true" value="*000000,801#"></td>
		<td><input type="submit" name="send5" value="Send #5"></td><td>(query for IMEI)</td>
	</tr>
		</tr>
		<td>#5</td>
		<td><input type="text" name="string6" readonly="true" value="*000000,990,099#"></td>
		<td><input type="submit" name="send6" value="Send #6"></td><td>(factory reset the device)</td>
	</tr>
	</tr>
		<td>#6</td>
		<td><input type="text" name="string7" value=""></td>
		<td><input type="submit" name="send7" value="Send #7"></td><td>(custom)</td>
	</tr>
	</form>
</table>

</body>
</html>