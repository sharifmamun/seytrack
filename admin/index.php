<html>
<head>
	<title>SeyTrackAdmin | index</title>
</head>
<body>
	<h1><a href="index.php">SeyTrackAdmin</a></h1>
	<hr>
	<ul>
		<li><a href="add_user.php">Add user</a>
		<li><a href="list_users.php">List/Edit users</a>
		<li><a href="add_device.php">Add device</a> (add a new device to the seytrack database)
		<li><a href="setup_device.php">Set up device</a> (send setup SMS commands to point a device at seytrack)
		<li><a href="list_devices.php">List/Edit devices</a> (also fuel graph helper)
		<li><a href="wizard.php">Add device wizard</a>
		<li><a href="reset.php">Reset device</a>
		<li><a href="reset_devices.php">Mass reset wizard</a>
		<li><a href="geocoder.html">Geocoder</a>
		<li><a href="geocoder_regions.html">Regions Geocoder</a>

	</ul>
	Other useful things:
	<ul>
		<li><a href="https://docs.google.com/a/bieh.net/spreadsheet/ccc?key=0Au8NvFDqmD5ldDVRZEVsOVppZnUtWXozYWVST3dWYWc&pli=1#gid=0">New device list</a> (that the wizard uses)
		<li><a href="https://docs.google.com/a/bieh.net/spreadsheet/ccc?key=0Au8NvFDqmD5ldDFuWFdSdlYyZUU1d3k1bEdoY3dHcmc">Old device list</a></li>
		<li><a href="https://docs.google.com/viewer?a=v&q=cache:3ZNYu8Q1hicJ:doc.diytrade.com/docdvr/464881/11069317/1258049050.pdf+&hl=en&pid=bl&srcid=ADGEESgpYDg7srk2wGigPDJOzf0Ul6d4YOj34JmIV9WkjL2Gdv2SWT50nyz3MJOF_PBEu5WtZxOSc0vJydjd7mTQKgGRhM-7Zo8LCcE1qTLaNB9DW4tNIoCWGFlLaoWs2s-xgDe2ilVw&sig=AHIEtbQQypSTH4UP7HwSZn2hvteReoK8xw">AVL02 manual</a></li>
		<li><a href="http://www.tzonedigital.com/Uploads/Download/051aff3013d140b8a4d6b297a0bcc6c1.pdf">Another AVL02 manual</a>
	</ul>
</body>
</html>