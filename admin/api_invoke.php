<?php
require("utils.php");

$params = "";

foreach ($_REQUEST as $key => $value) {
	$params = $params.$key."=".urlencode($value)."&";
}

$url = BASE_URL . "/api/api.py?". $params;

echo "Executing API request: <a href=\"".$url."\">".$url."</a>...";

$r = json_decode(file_get_contents($url));


if($r->status){
	echo "okay!";
}else{
	echo "failed!";
}

echo "<br><br><h3><a href=\"".$_SERVER['HTTP_REFERER']."\">Continue...</a></h3>";

?>