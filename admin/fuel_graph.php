<html>
<head>
	<title>SeyTrackAdmin | fuel voltage helper</title>

<script src="../web/bootstrap/js/jquery.js"></script>

 <script type="text/javascript" src="https://www.google.com/jsapi"></script>


<script type="text/javascript">

<?php
	require("utils.php");
	$auto = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=get_fuel_limits&id=".$_REQUEST["id"]));
?>


// Load the Visualization API and the piechart package.
google.load('visualization', '1.0', {'packages':['corechart']});


function make_graph(){

	var timeend = <?php if(isset($_REQUEST["timeend"])){ echo $_REQUEST["timeend"]; }else{echo "0"; } ?> 
	var timestart = <?php if(isset($_REQUEST["timestart"])){ echo $_REQUEST["timestart"]; }else{echo -(60 * 60 * 24 * 7); } ?> 

	//var timestart = timeend - (60 * 60 * 24 * 7);
	var device = <?php echo $_REQUEST['id'];?>;
	var fuel_min = <?php if(isset($_REQUEST["fuel_min"])){ echo $_REQUEST["fuel_min"]; }else{echo $auto->result->fuel_min; } ?> 
	var fuel_max = <?php if(isset($_REQUEST["fuel_max"])){ echo $_REQUEST["fuel_max"]; }else{echo $auto->result->fuel_max; } ?> 
	var smooth_fuel = <?php if(isset($_REQUEST["smooth_fuel"])){ echo $_REQUEST["smooth_fuel"]; }else{echo "-1"; } ?> 
	var inverse_fuel = <?php if(isset($_REQUEST["inverse_fuel"])){ echo $_REQUEST["inverse_fuel"]; }else{echo "-1"; } ?> 
	var chunk_size = <?php if(isset($_REQUEST["chunk_size"])){ echo $_REQUEST["chunk_size"]; }else{echo "-1"; } ?> 
	var min_rise = <?php if(isset($_REQUEST["min_rise"])){ echo $_REQUEST["min_rise"]; }else{echo "-1"; } ?> 


	url = "../api/api.py?op=get_report_data&attrib=fuel"+
            "&devices="+device+"&time=custom|"+timestart+"|"+timeend+"&min_rise="+min_rise+"&fuel_max="+fuel_max+"&fuel_min="+fuel_min+"&smooth_fuel="+smooth_fuel+"&inverse_fuel="+inverse_fuel+"&chunk_size="+chunk_size;

    $.getJSON(url, function(data) {
        success = data["status"];

        if(success == false){
            alert("Failed to get graph data");
            return;
        }


        //Setup vars
        var options = {'title':data["result"]["title"], 'vAxis':{'minValue':0, 'maxValue':100}};
        
        report_title = data["result"]["title"];
        attrib = data["result"]["attrib"];

        $("#report_title").html("<b>" + data["result"]["report_title"] + "</b><br>");
       
        if(data["result"]["data"].length <= 1 || data["result"]["data"][1].length == 0){
            $("#chart").html("No data for that time period!");
        }else{
            var chart_data = google.visualization.arrayToDataTable(
                                            data["result"]["data"]);

            //Create the chart
            var chart = new google.visualization.AreaChart(document.getElementById('chart'));
            
            chart.draw(chart_data, options);

        }
   });
}

function init(){

	make_graph();
}

</script>

</head>
<body onload="init()">
	<h1><a href="index.php">SeyTrackAdmin</a> | fuel voltage helper</h1>
	<?php

		echo "Maximum and minumum historical readings:<br>";
		
		echo "- Minimum: ".$auto->result->fuel_min."</br>";
		echo "- Maximum: ".$auto->result->fuel_max."</br>";

	function setting($key){

		$api = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=device_setting&id=".$_REQUEST["id"]."&key=".$key));

		echo "<form action=\"api_invoke.php\" method=\"get\">";
		echo "".$key." ";
		echo "<input name=\"value\" type=\"text\" value=\"".$api->result."\">";
		echo "<input name=\"id\" type=\"hidden\" value=\"".$_REQUEST["id"]."\">";
		echo "<input name=\"op\" type=\"hidden\" value=\"device_setting\">";
		echo "<input name=\"key\" type=\"hidden\" value=\"".$key."\">";
		echo "<input type=\"submit\" value=\"update\">";
		echo "</form>";

	}

	?>

	<br>Current values of max_fuel and min_fuel in database (used on seytrax.com):<br>


	<?php 

	setting("fuel_min");
	
	setting("fuel_max");
	?>

</form>

	<hr>

	<b>Graph settings:</b><br>

	<form action="fuel_graph.php" method="get">
		fuel_min: <input type="text" name="fuel_min" value=<?php if(isset($_REQUEST["fuel_min"])){ echo $_REQUEST["fuel_min"]; }else{echo $auto->result->fuel_min; } ?>><br>
		fuel_max: <input type="text" name="fuel_max" value=<?php if(isset($_REQUEST["fuel_max"])){ echo $_REQUEST["fuel_max"]; }else{echo $auto->result->fuel_max; } ?>><br>
		inverse_fuel (0/1): <input type="text" name="inverse_fuel" value=<?php if(isset($_REQUEST["inverse_fuel"])){ echo $_REQUEST["inverse_fuel"]; }else{echo "-1"; } ?>><br>
		smooth_fuel (0/1): <input type="text" name="smooth_fuel" value=<?php if(isset($_REQUEST["smooth_fuel"])){ echo $_REQUEST["smooth_fuel"]; }else{echo "-1"; } ?>><br>
		chunk_size (0-10ish): <input type="text" name="chunk_size" value=<?php if(isset($_REQUEST["chunk_size"])){ echo $_REQUEST["chunk_size"]; }else{echo "-1"; } ?>><br>
		min_rise (%): <input type="text" name="min_rise" value=<?php if(isset($_REQUEST["min_rise"])){ echo $_REQUEST["min_rise"]; }else{echo "-1"; } ?>><br>
		timestart (unix time): <input type="text" name="timestart" value=<?php if(isset($_REQUEST["timestart"])){ echo $_REQUEST["timestart"]; }else{echo - (60*60*24*7); } ?>><br>
		timeend (unix time): <input type="text" name="timeend" value=<?php if(isset($_REQUEST["timeend"])){ echo $_REQUEST["timeend"]; }else{echo "0"; } ?>><br>


		<input type="hidden" name="id" value="<?php echo $_REQUEST['id']; ?>">
		<input type="submit" value="redraw graph">
	</form>



	<div id="report_title"></div>

	<div id="chart" style="height:400px">loading chart...</div>
	

<?php


?>
</body>
</html>