
<html>
<head>
	<title>SeyTrackAdmin | list devices</title>
</head>
<body>
	<h1><a href="index.php">SeyTrackAdmin</a> | list devices</h1>
	<hr>

<?php

require("utils.php");

$PAGE_SIZE = 20;

if(isset($_REQUEST["delid"])){
	echo "Deleting ".$_REQUEST["delid"];

	$api = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=del_device&id=".$_REQUEST["delid"]));
}

if(isset($_REQUEST["clear"])){
	echo "<br>Clearing old data: ";

	$api = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=del_history&before=".$_REQUEST["clear"]));

	echo "okay<br><br>";
}
?>

	
	

<?php
	if(!isset($_REQUEST["ex"])){
		echo '(<a href="?ex=true&start=0">View extended information</a>)<br>';
	}else{

		echo '(<a href="?ex=true&start='.($_REQUEST["start"]-$PAGE_SIZE).'">Prev page</a>) ';

		echo $_REQUEST["start"] . " to " . ($_REQUEST["start"] + $PAGE_SIZE);

		echo ' (<a href="?ex=true&start='.($_REQUEST["start"]+$PAGE_SIZE).'">Next page</a>)<br>';

	}

	echo '<table border="1" cellpadding="10">';


	require("utils.php");

	$users = None;

	if(isset($_REQUEST["ex"])){
		$users = json_decode(file_get_contents(BASE_URL . 
			"/api/api.py?op=device_list_ex&start=".$_REQUEST["start"]."&end=".($_REQUEST["start"]+$PAGE_SIZE)));		
	}else{
		$users = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=device_list"));
	}

	foreach ($users->result as $key => $value) {
		echo "<tr><td>".$value->name." (".$value->descr;
		echo ")</td><td> id".$value->id.", ".$value->owner."</td>";

		if(isset($_REQUEST["ex"])){

			if($value->last_update_diff > (60 * 60 * 12)){
				echo "<td><b>".$value->last_update."</b></td>";
			}else{
				echo "<td>".$value->last_update."</td>";
			}

		}

		echo "<td><a href=\"edit_device.php?id=";
		echo $value->id."\">edit</a> | <a href=\"fuel_graph.php?id=";
		echo $value->id."\">Fuel</a> | <a href=\"list_devices.php?delid=";
		echo $value->id."\">Del</a> | <a href=\"reset.php?id=";
		echo $value->id."\">Reset</a></td></tr>";
	}

?>
	</table>

	<br>
	Clear old data:<br>
	<a href="list_devices.php?clear=2592000">Older than a month</a>, 
	<a href="list_devices.php?clear=1209600">Older than a fortnight</a>,
	<a href="list_devices.php?clear=604800">Older than a week</a>


</body>
</html>