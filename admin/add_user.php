<html>
<head>
	<title>SeyTrackAdmin | Add user</title>
</head>
<body>
	<h1><a href="index.php">SeyTrackAdmin</a> | add user</h1>
	<hr>
	<form action="api_invoke.php?op=user_add" method="get">
		<table border="0">
		<tr><td>Name:</td><td> <input type="text" name="name"></td></tr>
		<tr><td>Email:</td><td><input type="text" name="email"></td></tr>
		<tr><td>Password:</td><td><input type="text" name="password"></td></tr>
		<tr><td>UserType:</td><td><input type="text" name="usertype"> (0 for normal user, 1 for alias)</td></tr>
		<tr><td>UserAlias:</td><td><input type="text" name="useralias"> (the ID of the master user if this user is an alias, or 0 for a normal user)</td></tr>

		<tr><td></td>
		<td>
		<input type="hidden" name="op" value="user_add">
		<input type="submit" value="Add user">
		</td></tr>
</table>

</body>
</html>