<html>
<head>
	<title>SeyTrackAdmin | Add device wizard</title>

	<style>
	select.icon-menu option {
	background-repeat:no-repeat;
	background-position:bottom left;
	padding-left:30px;
	}
	</style>
</head>

<?php
	require("utils.php");
?>

<body>
	<h4><a href="index.php">SeyTrackAdmin</a> | add device wizard</h4>

	<form action="../api/api.py" method="get">

		<table>

		<tr><td>

			User:

		</td>
		<td>

			<select name="uid">
			<?php 
			$users = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=user_list"));

			foreach ($users->result as $key => $value) {
				echo "<option value='".$value->id."'>".$value->name."</option>";
			}
			?>
			</select>

		</td></tr>


		</td></tr>

		<input type="hidden" name="gpstype" value="0">

		<tr><td>

			Type:

		</td><td>

			<select name="vehicletype" id="vehicletype" class="icon-menu" onchange="set_marker()">
			<?php 
			
			for($i = 0; $i <= 35; $i++) {
				echo "<option style='height:32px; background-image:url(../web/markers/black/vehicle$i.png);' value='".$value->id."'>".$i."</option>";
			}
			?>
			</select>
			<span id="vehicleimg"></span>



		</td></tr>

		<tr><td>Name:</td><td><input type="text" name="name"></td></tr>
		<tr><td>Descr:</td><td><input type="text" name="descr"></td></tr>
		<tr><td>IMEI: </td><td><input type="text" name="imei" value="359772037"></td></tr>
		<tr><td>Device Ph: </td><td><input type="text" name="phone" value="248"></td></tr>
		<tr><td>Your Ph: </td><td><input type="text" name="srcphone" value="2482522248"></td></tr>
		
		<input type="hidden" name="min_fuel" value="0">
		<input type="hidden" name="max_fuel" value="0">



		<tr><td></td><td><input type="hidden" name="op" value="add_device_wizard">
		<br><br><br><br>
		<input type="submit" value="Add the device"></td></tr>

	</table>

</form>

</body>
<script type="text/javascript">

function set_marker(){
	d = document.getElementById("vehicletype").selectedIndex;

	document.getElementById("vehicleimg").innerHTML = "<img src='../web/markers/black/vehicle" + d + ".png'>";	
}
</script>
</html>