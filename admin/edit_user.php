<html>
<head>
	<title>SeyTrackAdmin | edit user</title>
</head>
<body>
	<h1><a href="index.php">SeyTrackAdmin</a> | edit user</h1>
	<hr>
	
	<table border="1" cellpadding="10">

<?php
	require("utils.php");

	function setting($key){

		$api = json_decode(file_get_contents(BASE_URL . "/api/api.py?op=user_setting&id=".$_REQUEST["id"]."&key=".$key));

		echo "<tr><td>".$key."</td><td>";
		echo "<form action=\"api_invoke.php\" method=\"get\">";
		echo "<input name=\"value\" type=\"text\" value=\"".$api->result."\">";
		echo "<input name=\"id\" type=\"hidden\" value=\"".$_REQUEST["id"]."\">";
		echo "<input name=\"op\" type=\"hidden\" value=\"user_setting\">";
		echo "<input name=\"key\" type=\"hidden\" value=\"".$key."\">";
		echo "</td>";
		echo "<td>";
		echo "<input type=\"submit\" value=\"update\">";
		echo "</td>";
		echo "</form>";
		echo "</tr>";

	}

	setting("name");
	setting("email");
	setting("password");
	setting("phone");
	setting("usertype");
	setting("useralias");


?>
	</table>
</body>
</html>