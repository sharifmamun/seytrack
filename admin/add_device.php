<html>
<head>
	<title>SeyTrackAdmin | Add device</title>
</head>
<body>
	<h1><a href="index.php">SeyTrackAdmin</a> | add device</h1>

	This adds a device to SeyTrack. It doesn't send the SMS setup commands to the device. 
	For that, you may want to go <a href="setup_device.php">here</a> after completing this page.
	<hr>
	<form action="../api/api.py" method="get">

		<table>

		<tr><td>User id:</td><td><input type="text" name="uid"> (get this from <a href="list_users.php">list_users</a>)</td></tr>
		<tr><td>GPS type:</td><td><input type="text" name="gpstype"> (probably 0)</td></tr>
		<tr><td>Vehicle type:</td><td><input type="text" name="vehicletype"> (ie: 5 for vehicle5.png in <a href="../web/markers/black">this folder</a>)</td></tr>
		<tr><td>Vehicle name</td><td><input type="text" name="name"></td></tr>
		<tr><td>Descr:</td><td><input type="text" name="descr"></td></tr>
		<tr><td>IMEI: </td><td><input type="text" name="imei"></td></tr>
		<tr><td>Fuel empty: </td><td><input type="text" name="min_fuel"> (volts to gauge when fuel empty. ie: 100 for 1.0 volts, 0 to disable)</td></tr>
		<tr><td>Fuel full: </td><td><input type="text" name="max_fuel"> (volts to gauge when fuel full. ie: 850 for 8.50 volts, 0 to disable)</td></tr>

		<tr><td></td><td><input type="hidden" name="op" value="add_device">
		<input type="submit" value="Add the device"></td></tr>

	</table>

</form>

Also make sure to add this to the <a href="https://docs.google.com/a/bieh.net/spreadsheet/ccc?key=0Au8NvFDqmD5ldDFuWFdSdlYyZUU1d3k1bEdoY3dHcmc">Google Doc</a>.
</body>
</html>